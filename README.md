## Content

This contains the code to run the dualnetwork, axisymmetric
conjugate heat transfer simulations in a wavy channel, 3D
heat conduction simulations on unit cells from the paper

T. Koch, K. Weishaupt, J. Müller, B. Weigand, R.Helmig (2021) A (dual) network model for heat transfer in porous media, Transport in Porous Media doi: 10.1007/s11242-021-01602-5

This is a so-called dune module that requires installation of
some other dune modules and the module dumux. 
The installation script contains the dependecies in the correct version.

## Installation

   The easiest way of installation is to use the script `install_dumux-dualnetwork.sh` provided in this repository.
   Using `wget`, you can simply install all dependent modules by typing:

   ```sh
   wget https://git.iws.uni-stuttgart.de/dumux-pub/Koch2021a.git/install_dumux-dualnetwork.sh
   chmod u+x install_dumux-dualnetwork.sh
   ./install_dumux-dualnetwork.sh
   ```

   This will create a sub-folder `DUMUX`, clone all modules into it, configure the entire project and build the applications contained in this module

## Usage

The applications are build by running
```
make -j4 build_tests
```
in the build-cmake folder of the dumux-dualnetwork module folder.

The executables can be found here
- appl/dualnetwork
- appl/dualnetwork/upscaling
- appl/upscale_3d
- appl/upscale_3d/spherepacking
- appl/wavychannel

You can run them and view the results with paraview.
There is also python script to generate a series of results
with different configurations.


## Installation with Docker 

Create a new folder in your favourite location and change into it

```bash
mkdir Koch2021a
cd Koch2021a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Koch2021a/-/raw/master/docker_dumux-dualnetwork.sh
```
Open the Docker Container
```bash
bash docker_dumux-dualnetwork.sh open
```
After the script has run successfully, you may build all executables

```bash
cd dumux/dumux-dualnetwork/build-cmake
make -j4 build_tests
```

They are located in the build-cmake folder according to the following paths:

- appl/dualnetwork
- appl/dualnetwork/upscaling
- appl/upscale_3d
- appl/upscale_3d/spherepacking
- appl/wavychannel

It can be executed with an input file, e.g.

```bash
cd appl/dualnetwork
./dualnetwork_heat params.input
```
