add_subdirectory(upscaling)

add_input_file_links()
dune_symlink_to_source_files(FILES grids)

dune_add_test(NAME dualnetwork_heat
              SOURCES main.cc)
