// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test for the pore network model
 */
#include <config.h>

#include <iostream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/porenetwork/common/pnmvtkoutputmodule.hh>
#include <dumux/io/grid/porenetwork/gridmanager.hh>
#include <dumux/io/grid/porenetwork/subgriddata.hh>

#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/porenetwork/common/boundaryflux.hh>

#include "properties.hh"
#include "problem_solid.hh"
#include "problem_void.hh"
#include "upscalinghelper.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    ////////////////////////////////////////////////////////////
    // parse the command line arguments and input file
    ////////////////////////////////////////////////////////////

    // parse command line arguments
    Parameters::init(argc, argv);

    //////////////////////////////////////////////////////////////////////
    // try to create a grid (from the given grid file or the input file)
    /////////////////////////////////////////////////////////////////////

    using HostGridManager = PoreNetwork::GridManager<3>;
    HostGridManager hostGridManager;
    hostGridManager.init();

    const auto hostGridView = hostGridManager.grid().leafGridView();
    const auto hostGridData = hostGridManager.getGridData();

    using SubGrid = Dune::SubGrid<1, typename HostGridManager::Grid>;
    using SubGridManager = GridManager<SubGrid>;

    auto elementSelectorVoid = [&](const auto& element)
    {
        return hostGridData->getParameter(element, "ThroatDomainType") == 0;
    };

    auto elementSelectorSolid = [&](const auto& element)
    {
        return hostGridData->getParameter(element, "ThroatDomainType") == 1;
    };

    // for debugging only
    auto elementSelectorCoupling = [&](const auto& element)
    {
        return hostGridData->getParameter(element, "ThroatDomainType") == 2;
    };

    auto& hostGrid = hostGridManager.grid();

    SubGridManager subgridManagerVoid; subgridManagerVoid.init(hostGrid, elementSelectorVoid);
    SubGridManager subgridManagerSolid; subgridManagerSolid.init(hostGridManager.grid(), elementSelectorSolid);
    SubGridManager subgridManagerCoupling; subgridManagerCoupling.init(hostGridManager.grid(), elementSelectorCoupling);
    const auto voidGridView = subgridManagerVoid.grid().leafGridView();
    const auto solidGridView = subgridManagerSolid.grid().leafGridView();
    const auto couplingGridView = subgridManagerCoupling.grid().leafGridView(); // for debugging only

    std::cout << "Void grid has " << voidGridView.size(0) << " elements" << std::endl;
    std::cout << "Solid grid has " << solidGridView.size(0) << " elements" << std::endl;

    using SubGridData = PoreNetwork::SubGridData<HostGridManager::Grid, SubGridManager::Grid>;
    auto voidGridData = std::make_shared<SubGridData>(subgridManagerVoid.grid(), hostGridData);
    auto solidGridData = std::make_shared<SubGridData>(subgridManagerSolid.grid(), hostGridData);

    using SolidTypeTag = Properties::TTag::PNMSolidModel;
    using VoidTypeTag = Properties::TTag::PNMVoidModel;

    // create the finite volume grid geometry
    using VoidGridGeometry = GetPropType<VoidTypeTag, Properties::GridGeometry>;
    auto voidGridGeometry = std::make_shared<VoidGridGeometry>(voidGridView);
    voidGridGeometry->update(*voidGridData);

    using SolidGridGeometry = GetPropType<SolidTypeTag, Properties::GridGeometry>;
    auto solidGridGeometry = std::make_shared<SolidGridGeometry>(solidGridView);
    solidGridGeometry->update(*solidGridData);

    // the spatial parameters
    using VoidSpatialParams = GetPropType<VoidTypeTag, Properties::SpatialParams>;
    auto voidSpatialParams = std::make_shared<VoidSpatialParams>(voidGridGeometry, *voidGridData);

    using SolidSpatialParams = GetPropType<SolidTypeTag, Properties::SpatialParams>;
    auto solidSpatialParams = std::make_shared<SolidSpatialParams>(solidGridGeometry, *solidGridData);

    // the coupling manager
    using Traits = MultiDomainTraits<SolidTypeTag, VoidTypeTag>;
    using CouplingManager = PNMHeatTransferCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>();
    using SolutionVector = typename Traits::SolutionVector;
    SolutionVector sol;

    // the problem (boundary conditions)
    using VoidProblem = GetPropType<VoidTypeTag, Properties::Problem>;
    auto voidProblem = std::make_shared<VoidProblem>(voidGridGeometry, voidSpatialParams, couplingManager);
    sol[CouplingManager::voidDomainIdx].resize(voidProblem->gridGeometry().numDofs());

    using SolidProblem = GetPropType<SolidTypeTag, Properties::Problem>;
    auto solidProblem = std::make_shared<SolidProblem>(solidGridGeometry, solidSpatialParams, couplingManager);
    sol[CouplingManager::solidDomainIdx].resize(solidProblem->gridGeometry().numDofs());

    const auto sideLengths = [&]()
    {
        using GlobalPosition = typename SolidGridGeometry::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;
        if (hasParam("Problem.SideLength"))
            return getParam<GlobalPosition>("Problem.SideLength");
        else
            return UpscalingHelper::getSideLengths(*solidGridGeometry);
    }();

    voidProblem->setSideLengths(sideLengths);
    solidProblem->setSideLengths(sideLengths);

    const auto directions = getParam<std::vector<int>>("Problem.Directions", std::vector<int>{0,1,2});

    // set the direction in which the pressure gradient will be applied
    solidProblem->setDirection(directions[0]);
    voidProblem->setDirection(directions[0]);

    // initialize the coupling manager
    couplingManager->init(solidProblem, voidProblem,
                          hostGridView, *hostGridData,
                          voidGridView, solidGridView, sol);

    voidProblem->applyInitialSolution(sol[CouplingManager::voidDomainIdx]);
    solidProblem->applyInitialSolution(sol[CouplingManager::solidDomainIdx]);

    // the grid variables
    using VoidGridVariables = GetPropType<VoidTypeTag, Properties::GridVariables>;
    auto voidGridVariables = std::make_shared<VoidGridVariables>(voidProblem, voidGridGeometry);
    voidGridVariables->init(sol[CouplingManager::voidDomainIdx]);

    using SolidGridVariables = GetPropType<SolidTypeTag, Properties::GridVariables>;
    auto solidGridVariables = std::make_shared<SolidGridVariables>(solidProblem, solidGridGeometry);
    solidGridVariables->init(sol[CouplingManager::solidDomainIdx]);

    // initialize the vtk output modules
    using Scalar = typename Traits::Scalar;
    using VoidVtkWriter = PoreNetwork::VtkOutputModule<
        VoidGridVariables, GetPropType<VoidTypeTag, Properties::FluxVariables>, decltype(sol[CouplingManager::voidDomainIdx])
    >;
    VoidVtkWriter voidVtkWriter(*voidGridVariables, sol[CouplingManager::voidDomainIdx],  voidProblem->name());
    GetPropType<VoidTypeTag, Properties::IOFields>::initOutputModule(voidVtkWriter);
    using SolidVtkWriter = PoreNetwork::VtkOutputModule<
        SolidGridVariables, GetPropType<SolidTypeTag, Properties::FluxVariables>, decltype(sol[CouplingManager::solidDomainIdx])
    >;
    SolidVtkWriter solidVtkWriter(*solidGridVariables, sol[CouplingManager::solidDomainIdx],  solidProblem->name());
    GetPropType<SolidTypeTag, Properties::IOFields>::initOutputModule(solidVtkWriter);

    using Scalar = typename Traits::Scalar;
    // the assembler for a stationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(solidProblem, voidProblem),
                                                 std::make_tuple(solidGridGeometry,
                                                                 voidGridGeometry),
                                                 std::make_tuple(solidGridVariables,
                                                                 voidGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // pass gridvars to void problem
    voidProblem->setGridVariables(voidGridVariables);

    // pass gridVars to coupling manager
    couplingManager->setGridVariables(std::make_tuple(solidGridVariables, voidGridVariables));

    // add dof indices and corresponding host grid vertex indices
    std::vector<std::size_t> voidDofIdx(voidGridView.size(1));
    std::vector<std::size_t> voidHostGridVertexIdx(voidGridView.size(1));
    std::vector<Scalar> voidSource(voidGridView.size(1));
    std::vector<Scalar> voidConvectionSource(voidGridView.size(1));
    for (const auto& v : vertices(voidGridView))
    {
        const auto vIdx = voidGridView.indexSet().index(v);
        voidDofIdx[vIdx] = vIdx;
        voidHostGridVertexIdx[vIdx] = hostGridView.indexSet().index(v.impl().hostEntity());
    }

    auto voidFVGeometry = localView(*voidGridGeometry);
    auto voidElemVolVars = localView(voidGridVariables->curGridVolVars());

    // set the reference transmissibilities
    for (const auto& element : elements(voidGridView))
    {
        voidFVGeometry.bind(element);
        voidElemVolVars.bind(element, voidFVGeometry, sol[CouplingManager::voidDomainIdx]);

        static const auto referenceElementIdx = getParamFromGroup<int>(voidProblem->paramGroup(), "Problem.ReferenceElementIdx", -1);
        const auto eIdx = voidGridGeometry->elementMapper().index(element);
        if (eIdx == referenceElementIdx)
        {
            for (const auto& scvf : scvfs(voidFVGeometry))
            {
                auto voidElemFluxVarsCache = localView(voidGridVariables->gridFluxVarsCache());
                voidElemFluxVarsCache.bind(element, voidFVGeometry, voidElemVolVars);

                using HeatConductionType = GetPropType<VoidTypeTag, Properties::HeatConductionType>;
                const auto refTrans = HeatConductionType::transmissibility(*voidProblem, element, voidFVGeometry, voidElemVolVars, scvf, voidElemFluxVarsCache);
                voidProblem->setInternalReferenceHeatTransmissibility(refTrans);
                std::cout << "Setting reference void transmissibility: " << refTrans << " at element " << eIdx << ", pos: " << element.geometry().center() << std::endl;
            }

            for (const auto& scv : scvs(voidFVGeometry))
            {
                const auto coordNum = voidGridGeometry->coordinationNumber(scv.dofIndex());
                if (coordNum != 6)
                    DUNE_THROW(Dune::InvalidStateException, "Throat is not internal");

                const auto& connections = couplingManager->couplingMapper().voidToSolidConnections(scv.dofIndex());
                for (const auto& connection : connections)
                {
                    const Scalar t = couplingManager->getConnectionTransmissiblity(CouplingManager::voidDomainIdx, connection, voidElemVolVars, scv);
                    std::cout << "conn has area " << connection.connectionArea << ", length " << connection.connectionLength << ", t " << t << std::endl;
                    voidProblem->setInternalReferenceHeatTransmissibilityCoupling(t);
                }
                break;
            }
        }
    }

    // calculate sources for output
    for (const auto& element : elements(voidGridView))
    {
        voidFVGeometry.bind(element);
        voidElemVolVars.bind(element, voidFVGeometry, sol[CouplingManager::voidDomainIdx]);
        using Indices = std::decay_t<decltype(voidElemVolVars)>::VolumeVariables::Indices;
        couplingManager->bindCouplingContext(CouplingManager::voidDomainIdx, element);

        for (const auto& scv : scvs(voidFVGeometry))
        {
            voidSource[scv.dofIndex()] += voidProblem->source(element, voidFVGeometry, voidElemVolVars, scv)[Indices::temperatureIdx] * scv.volume();

            if (couplingManager->isCoupledPore(CouplingManager::voidDomainIdx, scv.dofIndex()))
                voidConvectionSource[scv.dofIndex()] += couplingManager->convectionSource(CouplingManager::voidDomainIdx, element, voidFVGeometry,
                                                                                          voidElemVolVars, scv) * scv.volume();
        }
    }

    voidVtkWriter.addField(voidDofIdx, "dofIdx", VoidVtkWriter::FieldType::vertex);
    voidVtkWriter.addField(voidHostGridVertexIdx, "hostGridVertexIdx", VoidVtkWriter::FieldType::vertex);
    voidVtkWriter.addField(voidSource, "source", VoidVtkWriter::FieldType::vertex);
    voidVtkWriter.addField(voidConvectionSource, "convectionSource", VoidVtkWriter::FieldType::vertex);
    voidVtkWriter.addField(voidGridGeometry->poreVolume(), "poreVolume", VoidVtkWriter::FieldType::vertex);
    voidVtkWriter.addField(voidGridGeometry->throatCrossSectionalArea(), "throatArea", VoidVtkWriter::FieldType::element);
    voidVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& elemFluxVarsCache){ return fluxVars.heatConductionFlux(); }, "heatConductionFlux");

    std::vector<std::size_t> solidDofIdx(solidGridView.size(1));
    std::vector<std::size_t> solidHostGridVertexIdx(solidGridView.size(1));
    std::vector<Scalar> solidSource(solidGridView.size(1));
    for (const auto& v : vertices(solidGridView))
    {
        const auto vIdx = solidGridView.indexSet().index(v);
        solidDofIdx[vIdx] = vIdx;
        solidHostGridVertexIdx[vIdx] = hostGridView.indexSet().index(v.impl().hostEntity());
    }

    auto solidFVGeometry = localView(*solidGridGeometry);
    auto solidElemVolVars = localView(solidGridVariables->curGridVolVars());
    for (const auto& element : elements(solidGridView))
    {
        solidFVGeometry.bind(element);
        solidElemVolVars.bind(element, solidFVGeometry, sol[CouplingManager::solidDomainIdx]);

        for (const auto& scv : scvs(solidFVGeometry))
            solidSource[scv.dofIndex()] += solidProblem->source(element, solidFVGeometry, solidElemVolVars, scv) * scv.volume();
    }

    solidVtkWriter.addField(solidDofIdx, "dofIdx", SolidVtkWriter::FieldType::vertex);
    solidVtkWriter.addField(solidHostGridVertexIdx, "hostGridVertexIdx", SolidVtkWriter::FieldType::vertex);
    solidVtkWriter.addField(solidSource, "source", SolidVtkWriter::FieldType::vertex);
    solidVtkWriter.addField(solidGridGeometry->throatCrossSectionalArea(), "throatArea", SolidVtkWriter::FieldType::element);
    solidVtkWriter.addField(solidGridGeometry->poreVolume(), "poreVolume", SolidVtkWriter::FieldType::vertex);
    solidVtkWriter.addFluxVariable([](const auto& fluxVars, const auto& elemFluxVarsCache){ return fluxVars.heatConductionFlux(); }, "heatConductionFlux");

    voidVtkWriter.write(0);
    solidVtkWriter.write(0);
    std::fill(voidSource.begin(), voidSource.end(), 0.0);
    std::fill(voidConvectionSource.begin(), voidConvectionSource.end(), 0.0);
    std::fill(solidSource.begin(), solidSource.end(), 0.0);

    // for debugging
    Dune::VTKWriter<std::decay_t<decltype(couplingGridView)>> couplingDebugWriter(couplingGridView);
    std::vector<std::size_t> couplingDebugVertexIndices(couplingGridView.size(1));
    std::vector<double> couplingDebugVertexTemperature(couplingGridView.size(1));
    std::vector<std::size_t> couplingDebugRegion(couplingGridView.size(1));
    std::vector<std::size_t> couplingDebugElementIndices(couplingGridView.size(0));
    // std::vector<std::size_t> couplingDebugElementIndices(couplingGridView.size(0));
    for (const auto& v : vertices(couplingGridView))
    {
        const auto vIdx = couplingGridView.indexSet().index(v);
        const auto hostVertexIdx = hostGridView.indexSet().index(v.impl().hostEntity());
        couplingDebugVertexIndices[vIdx] = hostVertexIdx;

        if (hostVertexIdx == 13)
            std::cout << "yay" << std::endl;

        if (couplingManager->couplingMapper().solidHostToSubVertexIdxMap().count(hostVertexIdx))
        {
            couplingDebugRegion[vIdx] = 0;
            const auto solidVertexIdx = couplingManager->couplingMapper().solidHostToSubVertexIdxMap().at(hostVertexIdx);
            couplingDebugVertexTemperature[vIdx] = sol[CouplingManager::solidDomainIdx][solidVertexIdx];
        }
        else
        {
            couplingDebugRegion[vIdx] = 1;
            const auto voidVertexIdx = couplingManager->couplingMapper().voidHostToSubVertexIdxMap().at(hostVertexIdx);
            couplingDebugVertexTemperature[vIdx] = sol[CouplingManager::voidDomainIdx][voidVertexIdx][1];
        }
    }
    for (const auto& e : elements(couplingGridView))
    {
        couplingDebugElementIndices[couplingGridView.indexSet().index(e)] =  hostGridView.indexSet().index(e.impl().hostEntity());
    }

    couplingDebugWriter.addVertexData(couplingDebugVertexIndices, "hostGridVertexIdx");
    couplingDebugWriter.addVertexData(couplingDebugRegion, "isVoid");
    couplingDebugWriter.addVertexData(couplingDebugVertexTemperature, "T");
    couplingDebugWriter.addCellData(couplingDebugElementIndices, "hostGridElementIdx");



    couplingDebugWriter.write("debug_coupling");

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    const auto voidBoundaryFlux = PoreNetwork::BoundaryFlux(
        *voidGridVariables, assembler->localResidual(CouplingManager::voidDomainIdx), sol[CouplingManager::voidDomainIdx]
    );
    const auto solidBoundaryFlux = PoreNetwork::BoundaryFlux(
        *solidGridVariables, assembler->localResidual(CouplingManager::solidDomainIdx), sol[CouplingManager::solidDomainIdx]
    );

    std::cout << "\n\n\n ************************************************ \n\n\n" << std::endl;

    // This procedure is now repeated for the number of refinements as specified
    // in the input file.
    for (int dimIdx : directions)
    {
        // reset the solution
        sol = 0;

        // set the direction in which the pressure gradient will be applied
        solidProblem->setDirection(dimIdx);
        voidProblem->setDirection(dimIdx);

        // solve problem
        nonLinearSolver.solve(sol);

        for (const auto& element : elements(voidGridView))
        {
            voidFVGeometry.bind(element);
            voidElemVolVars.bind(element, voidFVGeometry, sol[CouplingManager::voidDomainIdx]);
            using Indices = std::decay_t<decltype(voidElemVolVars)>::VolumeVariables::Indices;

            couplingManager->bindCouplingContext(CouplingManager::voidDomainIdx, element);

            for (const auto& scv : scvs(voidFVGeometry))
            {
                voidSource[scv.dofIndex()] += voidProblem->source(element, voidFVGeometry, voidElemVolVars, scv)[Indices::temperatureIdx] * scv.volume();

                if (couplingManager->isCoupledPore(CouplingManager::voidDomainIdx, scv.dofIndex()))
                    voidConvectionSource[scv.dofIndex()] += couplingManager->convectionSource(CouplingManager::voidDomainIdx, element, voidFVGeometry,
                                                                                              voidElemVolVars, scv) * scv.volume();
            }
        }

        for (const auto& element : elements(solidGridView))
        {
            solidFVGeometry.bind(element);
            solidElemVolVars.bind(element, solidFVGeometry, sol[CouplingManager::solidDomainIdx]);

            for (const auto& scv : scvs(solidFVGeometry))
                solidSource[scv.dofIndex()] += solidProblem->source(element, solidFVGeometry, solidElemVolVars, scv) * scv.volume();
        }

        // write the vtu file for the given direction
        voidVtkWriter.write(dimIdx+1);
        solidVtkWriter.write(dimIdx+1);

        // calculate the permeability
        static const bool getDarcyPermeability = getParam<bool>("Problem.GetDarcyPermeability", false);
        if (getDarcyPermeability)
        {
            std::cout << "\nDarcy permeability " << std::endl;
            const Scalar totalFluidMassFlux = voidBoundaryFlux.getFlux(std::vector<int>{voidProblem->outletPoreLabel()})[0];
            UpscalingHelper::getDarcyPermeability(*voidProblem, totalFluidMassFlux);
        }

        // calculate the heat cond.
        static const bool getFluidHeatCond = getParam<bool>("Problem.GetFluidHeatConductivity", false);
        if (getFluidHeatCond)
        {
            std::cout << "\n\n\nFluid heat cond." << std::endl;
            const Scalar totalHeatFlux = voidBoundaryFlux.getFlux(std::vector<int>{voidProblem->outletPoreLabel()})[1];
            const Scalar lambdaEff = UpscalingHelper::getHeatConductivity(*voidProblem, totalHeatFlux);
            static const Scalar lambdaFluid = getParamFromGroup<Scalar>(voidProblem->paramGroup(), "2.Component.LiquidThermalConductivity");
            std::cout << "Ratio lambdaEff / lambdaFluid: " << lambdaEff / lambdaFluid;
        }

        static const bool getSolidHeatCond = getParam<bool>("Problem.GetSolidHeatConductivity", false);
        if (getSolidHeatCond)
        {
            // The solid and void problem consider the same properties needed for getting the heat conductivity.
            // To avoid code duplication, we just pass the void problem here.
            std::cout << "\n\n\nSolid heat cond." << std::endl;
            const Scalar totalHeatFlux = solidBoundaryFlux.getFlux(std::vector<int>{voidProblem->outletPoreLabel()})[0];
            const Scalar lambdaEff = UpscalingHelper::getHeatConductivity(*voidProblem, totalHeatFlux);
            static const Scalar lambdaSolid = getParamFromGroup<Scalar>(solidProblem->paramGroup(), "1.Component.SolidThermalConductivity");
            std::cout << "Ratio lambdaEff / lambdaSolid: " << lambdaEff / lambdaSolid;
        }

        static const bool getTotalEffectiveHeatCond = getParam<bool>("Problem.GetTotalEffectiveHeatConductivity", false);
        if (getTotalEffectiveHeatCond)
        {
            // The solid and void problem consider the same properties needed for getting the heat conductivity.
            // To avoid code duplication, we just pass the void problem here.
            std::cout << "\n\n\nCombined heat cond." << std::endl;
            const Scalar totalHeatFluxVoid = voidBoundaryFlux.getFlux(std::vector<int>{voidProblem->outletPoreLabel()})[1];
            const Scalar totalHeatFluxSolid = solidBoundaryFlux.getFlux(std::vector<int>{voidProblem->outletPoreLabel()})[0];
            const Scalar lambdaEff = UpscalingHelper::getHeatConductivity(*voidProblem, totalHeatFluxVoid + totalHeatFluxSolid);
            static const Scalar lambdaFluid = getParamFromGroup<Scalar>(voidProblem->paramGroup(), "2.Component.LiquidThermalConductivity");
            static const Scalar lambdaSolid = getParamFromGroup<Scalar>(solidProblem->paramGroup(), "1.Component.SolidThermalConductivity");
            const Scalar lambdaMax = std::max(lambdaFluid, lambdaSolid);
            std::cout << "Ratio lambdaCombinedEff / lambdaMax: " << lambdaEff / lambdaMax;
        }
    }

    std::cout << "\n\n\n ************************************************ \n\n\n" << std::endl;

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
