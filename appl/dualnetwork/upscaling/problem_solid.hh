// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_UPSCALING_HEAT_SUBPROBLEM_HH
#define DUMUX_UPSCALING_HEAT_SUBPROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/common/boundarytypes.hh>

namespace Dumux {

/*!
 * \brief Heat problem with multiple solid spheres
 */
template <class TypeTag>
class UpscalingSolidSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dimworld = GridGeometry::GridView::dimensionworld;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    template<class SpatialParams>
    UpscalingSolidSubProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                             std::shared_ptr<SpatialParams> spatialParams,
                             std::shared_ptr<const CouplingManager> couplingManager)
    : ParentType(gridGeometry, spatialParams,  "Solid"), couplingManager_(couplingManager), eps_(1e-7)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        temperatureIn_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletTemperature");
        temperatureGradient_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.TemperatureGradient");
        enableCoupling_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableCoupling", true);
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume& scv) const
    {
        BoundaryTypes bcTypes;

        if (isInletPore_(scv)|| isOutletPore_(scv))
        {
            bcTypes.setAllDirichlet();
        }
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        NumEqVector value = 0.0;

        static const bool fixed = getParamFromGroup<bool>(this->paramGroup(), "Problem.FixedTransSource", true);


        if (enableCoupling_ && couplingManager_->isCoupledPore(CouplingManager::solidDomainIdx, scv.dofIndex()))
        {
            if (fixed)
                value = couplingManager_->sourceWithFixedTransmissibility(CouplingManager::solidDomainIdx, element, fvGeometry,
                                                 elemVolVars, scv, CouplingManager::voidDomainIdx);
            else
                value = couplingManager_->conductionSource(CouplingManager::solidDomainIdx, element, fvGeometry,
                                                           elemVolVars, scv);
        }

        return value;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        PrimaryVariables values(0.0);
        static const bool fixedBoundaryVals = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseFixedBoundaryVals", false);
        if (fixedBoundaryVals)
        {
            if (isInletPore_(scv))
                values[Indices::temperatureIdx] = temperatureGradient_ * length_[direction_];
        }
        else if (isInletPore_(scv) || isOutletPore_(scv))
            values[Indices::temperatureIdx] = temperatureGradient_ * (couplingManager_->problem(CouplingManager::voidDomainIdx).gridGeometry().bBoxMax()[direction_] - scv.dofPosition()[direction_]);

        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& pos) const
    {
        PrimaryVariables values(0);

        static const bool useLinearInitialTemp = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseLinearInitialTemp", false);
        if (useLinearInitialTemp)
            values = temperatureGradient_ * (couplingManager_->problem(CouplingManager::voidDomainIdx).gridGeometry().bBoxMax()[direction_] - pos[direction_]);

        return values;
    }

    void setSideLengths(const GlobalPosition& len)
    { length_ = len; }

    const GlobalPosition& lengths() const
    { return length_; }

    /*!
     * \brief Sets the current direction in which the pressure gradient is applied.
     * \param directionIdx The index of the direction (0:x, 1:y, 2:z)
     */
    void setDirection(const int directionIdx)
    { direction_ = directionIdx; }

    int outletPoreLabel() const
    {
        static constexpr std::array<int, 3> label = {2, 4, 6};
        return label[direction_];
    }

    int inletPoreLabel() const
    {
        static constexpr std::array<int, 3> label = {1, 3, 5};
        return label[direction_];
    }

private:

    bool isInletPore_(const SubControlVolume& scv) const
    {
        return inletPoreLabel() == this->gridGeometry().poreLabel(scv.dofIndex());;
    }

    bool isOutletPore_(const SubControlVolume& scv) const
    {
        return outletPoreLabel() == this->gridGeometry().poreLabel(scv.dofIndex());;
    }

    std::string problemName_;
    Scalar temperatureIn_;
    Scalar temperatureGradient_;

    std::shared_ptr<const CouplingManager> couplingManager_;
    Scalar eps_;
    bool enableCoupling_;

    int direction_;
    GlobalPosition length_;

};
} // end namespace Dumux

#endif
