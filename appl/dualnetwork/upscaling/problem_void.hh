// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetwork/1p/model.hh>

#include <dumux/common/boundarytypes.hh>

namespace Dumux
{
template <class TypeTag>
class VoidSubProblem;

template <class TypeTag>
class VoidSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<GridView::dimension>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    template<class SpatialParams>
    VoidSubProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                   std::shared_ptr<SpatialParams> spatialParams,
                   std::shared_ptr<const CouplingManager> couplingManager)
    : ParentType(gridGeometry, spatialParams, "Void"), couplingManager_(couplingManager)
    {
        temperatureIn_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletTemperature");
        temperatureGradient_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.TemperatureGradient");
        pressureGradient_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PressureGradient");
        enableCoupling_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableCoupling", true);

        const auto internalDirichletDofs = getParamFromGroup<std::vector<std::size_t>>(this->paramGroup(), "Problem.UseInternalDirichlet", std::vector<std::size_t>{});
        if (!internalDirichletDofs.empty())
        {
            useInternalDirichlet_.resize(gridGeometry->numDofs(), false);
            for (const auto i : internalDirichletDofs)
            {
                if (i < useInternalDirichlet_.size())
                    useInternalDirichlet_[i] = true;
                else
                    DUNE_THROW(Dune::IOError, "Cant add DOF " << i << " as internal dirichlet DOF. Your network only has " <<  useInternalDirichlet_.size() << " DOFs.");
            }
        }
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        static const auto problemName = getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        return problemName;
    }

    void setGridVariables(std::shared_ptr<GridVariables> gridVars)
    {
        gridVars_ = gridVars;
    }

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolume& scv) const
    {
        BoundaryTypes bcTypes;

        if (isInletPore_(scv)|| isOutletPore_(scv) || (!useInternalDirichlet_.empty() && useInternalDirichlet_[scv.dofIndex()]))
            bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv) const
    {
        PrimaryVariables values(0.0);

        static const bool fixedBoundaryVals = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseFixedBoundaryVals", false);
        if (fixedBoundaryVals)
        {
            if (isInletPore_(scv))
            {
                values[Indices::pressureIdx] = pressureGradient_ * length_[direction_];
                values[Indices::temperatureIdx] = temperatureGradient_ * length_[direction_];
            }
        }
        else
        {
            if (isInletPore_(scv) || isOutletPore_(scv))
            {
                values[Indices::pressureIdx] = pressureGradient_ * (this->gridGeometry().bBoxMax()[direction_] - scv.dofPosition()[direction_]);
                values[Indices::temperatureIdx] = temperatureGradient_ * (this->gridGeometry().bBoxMax()[direction_] - scv.dofPosition()[direction_]);
            }
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        NumEqVector value(0.0);

        if (enableCoupling_ && couplingManager_->isCoupledPore(CouplingManager::voidDomainIdx, scv.dofIndex()))
        {
            static const bool fixed = getParamFromGroup<bool>(this->paramGroup(), "Problem.FixedTransSource", true);

            if (fixed)
                value[Indices::energyEqIdx] = couplingManager_->sourceWithFixedTransmissibility(CouplingManager::voidDomainIdx, element, fvGeometry,
                                                  elemVolVars, scv, CouplingManager::solidDomainIdx);
            else
                value[Indices::energyEqIdx] = couplingManager_->conductionSource(CouplingManager::voidDomainIdx,
                                                                                 element, fvGeometry, elemVolVars, scv);
        }

        return value;
     }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = 0;
        values[Indices::temperatureIdx] = 1;

        static const bool useLinearInitialTemp = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseLinearInitialTemp", false);
        if (useLinearInitialTemp)
        {
            const auto pos = vertex.geometry().center()[direction_];
            values[Indices::pressureIdx] = pressureGradient_ * (this->gridGeometry().bBoxMax()[direction_] - pos);
            values[Indices::temperatureIdx] = temperatureGradient_ * (this->gridGeometry().bBoxMax()[direction_] - pos);
        }

        return values;
    }

    /*!
     * \brief Sets the current direction in which the pressure gradient is applied.
     * \param directionIdx The index of the direction (0:x, 1:y, 2:z)
     */
    void setDirection(const int directionIdx)
    { direction_ = directionIdx; }

    int direction() const
    { return direction_; }

    void setSideLengths(const GlobalPosition& len)
    { length_ = len; }

    const GlobalPosition& sideLengths() const
    { return length_; }

    Scalar pressureGradient() const
    { return pressureGradient_; }

    Scalar temperatureGradient() const
    { return temperatureGradient_; }

    Scalar liquidDensity() const
    {
        static const Scalar liquidDensity = getParam<Scalar>("2.Component.LiquidDensity");
        return liquidDensity;
    }

    Scalar liquidDynamicViscosity() const
    {
        static const Scalar liquidDynamicViscosity = getParam<Scalar>("2.Component.LiquidKinematicViscosity") * liquidDensity();
        return liquidDynamicViscosity;
    }

    int outletPoreLabel() const
    {
        static constexpr std::array<int, 3> label = {2, 4, 6};
        return label[direction_];
    }

    int inletPoreLabel() const
    {
        static constexpr std::array<int, 3> label = {1, 3, 5};
        return label[direction_];
    }

    void setInternalReferenceHeatTransmissibility(const Scalar val)
    { internalReferenceHeatTransmissibility_ = val; }

    Scalar getInternalReferenceHeatTransmissibility() const
    { return internalReferenceHeatTransmissibility_; }

    void setInternalReferenceHeatTransmissibilityCoupling(const Scalar val)
    { internalReferenceHeatTransmissibilityCoupling_ = val; }

    Scalar getInternalReferenceHeatTransmissibilityCoupling() const
    { return internalReferenceHeatTransmissibilityCoupling_; }

    // \}

private:

    bool isInletPore_(const SubControlVolume& scv) const
    {
        return inletPoreLabel() == this->gridGeometry().poreLabel(scv.dofIndex());;
    }

    bool isOutletPore_(const SubControlVolume& scv) const
    {
        return outletPoreLabel() == this->gridGeometry().poreLabel(scv.dofIndex());;
    }

    std::shared_ptr<const CouplingManager> couplingManager_;
    std::shared_ptr<GridVariables> gridVars_;
    Scalar temperatureIn_;
    Scalar temperatureGradient_;
    Scalar pressureGradient_;
    bool enableCoupling_;

    int direction_;
    GlobalPosition length_;
    Scalar internalReferenceHeatTransmissibility_ = 0;
    Scalar internalReferenceHeatTransmissibilityCoupling_ = 0;

    std::vector<bool> useInternalDirichlet_;
};
} //end namespace

#endif
