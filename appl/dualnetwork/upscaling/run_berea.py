import os
import subprocess
import numpy as np

def run(dir, kappa, coupled):
    casename = 'dir_' + str(dir)
    casename += ("_coupled" if coupled else "_not_coupled")
    casename += '_' + str(kappa)
    print(casename)
    logFileName = casename + '.log'

    command = './dualnetwork_heat_upscaling params_berea.input '
    command += '-Grid.File ../grids/Berea_center_cropped_200_200_dual.dgf '
    command += '-1.Component.SolidThermalConductivity 1 '
    command += '-2.Component.LiquidThermalConductivity ' + str(kappa*1) + ' '
    command += '-Problem.EnableCoupling ' + ('1' if coupled else '0') + ' '
    command += '-Problem.Directions ' + str(dir) + ' '
    command += '-Vtk.OutputName ' + casename + ' '
    command += ' > ' + logFileName

    print(command)
    subprocess.call(command, shell=True)

    with open(logFileName) as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    for line in content:
        if line.startswith("Ratio lambdaEff / lambdaFluid:"):
            lambda_fluid_eff_over_lambda_fluid = line.split(" ")[-1]
        if line.startswith("Ratio lambdaEff / lambdaSolid:"):
            lambda_solid_eff_over_lambda_solid = line.split(" ")[-1]
        if line.startswith("Ratio lambdaCombinedEff / lambdaMax:"):
            lambda_combined_eff_over_lambda_max = line.split(" ")[-1]

    result = {'kappa':kappa, 'dir':dir, "lambdaT/lambdaFluid":lambda_fluid_eff_over_lambda_fluid, "lambdaA/":lambda_solid_eff_over_lambda_solid, "lambdaEff/lambdaMax":lambda_combined_eff_over_lambda_max}
    return result


direction = [0, 1, 2]
# kappas = [1,2]
kappas = np.unique(np.concatenate((
    np.logspace(-3.0, -1.0, 5, endpoint=True),
    np.logspace(-1.0, 0.0, 10, endpoint=True),
    np.logspace(0.0, 1.0, 10, endpoint=True),
    np.logspace(1.0, 3.0, 5, endpoint=True)
)))



coupled = True

result = np.empty(shape=(len(direction),len(kappas)))

for dir in direction:
    results_for_factor = []
    for kappa in kappas:
        results_for_factor.append(run(dir=dir, kappa=kappa, coupled=coupled)['lambdaEff/lambdaMax'])
        print("dir", dir, "with kappa", kappa, "done")
    result[dir] = np.array(results_for_factor)

np.save("results_dualnetwork_berea.npy", result)
