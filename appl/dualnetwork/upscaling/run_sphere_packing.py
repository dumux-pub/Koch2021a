import os
import subprocess
import numpy as np

def run(grid, kappa, coupled):
    casename = grid.split('_dual.dgf')[0]
    casename += ("_coupled" if coupled else "_not_coupled")
    casename += '_' + str(kappa)
    print(casename)
    sideLength = grid.split('_')[1]
    sideLength = str(sideLength) + "e-6 " + str(sideLength) + "e-6 " + str(sideLength) + "e-6"
    print(sideLength)
    logFileName = casename + '.log'

    overlapFactor = grid.split('_')[2]

    command = './dualnetwork_heat_upscaling params_sphere_packing.input '
    command += '-Grid.File ../grids/spherepacking/with_mc/' + grid + ' '
    command += '-Problem.OverlapFactor ' + str(overlapFactor) + ' '
    command += '-1.Component.SolidThermalConductivity 1 '
    command += '-2.Component.LiquidThermalConductivity ' + str(kappa*1) + ' '
    command += '-Problem.SideLength "' + sideLength + '" '
    command += '-Problem.EnableCoupling ' + ('1' if coupled else '0') + ' '
    command += '-Vtk.OutputName ' + casename + ' '
    command += ' > ' + logFileName

    # print(command)
    subprocess.call(command, shell=True)

    with open(logFileName) as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    for line in content:
        if line.startswith("Ratio lambdaEff / lambdaFluid:"):
            lambda_fluid_eff_over_lambda_fluid = line.split(" ")[-1]
        if line.startswith("Ratio lambdaEff / lambdaSolid:"):
            lambda_solid_eff_over_lambda_solid = line.split(" ")[-1]
        if line.startswith("Ratio lambdaCombinedEff / lambdaMax:"):
            lambda_combined_eff_over_lambda_max = line.split(" ")[-1]

    result = {'kappa':kappa, 'overlapFraction':overlapFactor, "lambdaT/lambdaFluid":lambda_fluid_eff_over_lambda_fluid, "lambdaA/":lambda_solid_eff_over_lambda_solid, "lambdaEff/lambdaMax":lambda_combined_eff_over_lambda_max}
    return result


gridFiles = ["3_294_0.980_dual.dgf", "3_288_0.960_dual.dgf", "3_282_0.940_dual.dgf", "3_276_0.920_dual.dgf", "3_270_0.900_dual.dgf", "3_264_0.880_dual.dgf", "3_258_0.860_dual.dgf", "3_252_0.840_dual.dgf", "3_246_0.820_dual.dgf", "3_240_0.800_dual.dgf"]

kappas = np.array([0.001, 0.01, 0.1, 0.3, 0.5, 0.75, 1.0, 1.25, 1.5, 3.0, 10.0, 100.0, 1000])


coupled = True

result = np.empty(shape=(len(gridFiles),len(kappas)))

for idx, case in enumerate(reversed(gridFiles)):
    results_for_factor = []
    for kappa in kappas:
        results_for_factor.append(run(grid=case, kappa=kappa, coupled=coupled)['lambdaEff/lambdaMax'])
        print("case", case, "with kappa", kappa, "done")
    result[idx] = np.array(results_for_factor)

np.save("results_dualnetwork.npy", result)
