#!/usr/bin/env python3

import numpy as np
import subprocess

inputfile = "spheres.in"
geofile = "spheres.geo"
# scale the entire domain by some factor
scaling = 1.0
clFactor = 0.3

# read data
data = np.genfromtxt(inputfile)
points, radii = data[:,0:3], data[:,3]

# write geo file
with open(geofile, "w") as geo:
    geo.write("Mesh.MshFileVersion = 2.0;\n")
    geo.write("Mesh.ScalingFactor = {};\n".format(scaling))
    geo.write("Mesh.CharacteristicLengthFactor = {};\n".format(clFactor))

    geo.write("SetFactory(\"OpenCASCADE\");\n")
    for i, (p, r) in enumerate(zip(points, radii)):
        geo.write("Sphere({}) = {{{}, {}, {}, {}, -Pi/2, Pi/2, 2*Pi}};\n".format(i, p[0], p[1], p[2], r))

    geo.write("BooleanUnion{{ Volume{{{}}}; Delete; }}{{ Volume{{{}}}; Delete; }}\n".format(0, ",".join(str(i) for i in range(1, len(points)))))

# run gmsh
subprocess.run(["gmsh", "-3", geofile])
subprocess.run(["gmsh", "-3", "-format", "vtk", geofile])
