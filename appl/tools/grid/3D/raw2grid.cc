/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Converts RAW CT-image file to gmsh / dgf / vtk
 */
#include <config.h>
#include <iostream>
#include <cmath>
#include <string>
#include <filesystem>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>

#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/common/parameters.hh>
#include <dumux/geometry/intersectspointgeometry.hh>


namespace Dumux {

// Reads the raw file into an array
template<typename Container>
Container readRawFileToContainer(const std::string& path)
{
    if (!std::filesystem::exists(path))
        DUNE_THROW(Dune::IOError, "File " << path << "not found");
    else
        std::cout << "Reading " << path << std::endl;

    // open the file:
    std::ifstream file(path, std::ios::binary);

    // skip new lines in binary mode:
    file.unsetf(std::ios::skipws);

    // get its size:
    file.seekg(0, std::ios::end);
    const std::size_t fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    Container fileData(fileSize);
    file.read((char*) &fileData[0], fileSize);
    std::cout << fileData.size() << " voxels found." << std::endl;
    return fileData;
}


/*!
 * \brief Mark elements connected to a given element
 */
template<class GridView, class Element, class T>
void recursivelyFindConnectedElements(const GridView& gridView,
                                      const Element& element,
                                      std::vector<T>& elementIsConnected,
                                      const T marker = 1)
 {
     // use iteration instead of recursion here because the recursion can get too deep
     std::stack<Element> elementStack;
     elementStack.push(element);
     while (!elementStack.empty())
     {
         auto e = elementStack.top();
         elementStack.pop();
         for (const auto& intersection : intersections(gridView, e))
         {
             if (!intersection.boundary())
             {
                 auto outside = intersection.outside();
                 auto nIdx = gridView.indexSet().index(outside);
                 if (!elementIsConnected[nIdx])
                 {
                     elementIsConnected[nIdx] = marker;
                     elementStack.push(outside);
                 }
             }
         }
     }
 }


template<class GridView, class Container, class GlobalPos>
std::vector<bool> getPermeableVoidElements(const GridView& gridView,
                                           const Container& v,
                                           const GlobalPos& lowerLeft,
                                           const GlobalPos& upperRight,
                                           const int dir,
                                           const int voidMarker)
{
    if (v.size() != gridView.size(0))
        DUNE_THROW(Dune::IOError, "RAW file contains " << v.size() << " voxels but your grid has " << gridView.size(0) << "cells");

    std::vector<bool> elementIsConnected(gridView.size(0), false);
    for (const auto& element : elements(gridView))
    {
        const auto eIdx = gridView.indexSet().index(element);

        // skip solid elements or those which have already been treated
        if (v[eIdx] != voidMarker || elementIsConnected[eIdx])
            continue;

        // try to find a seed from which to start the search process
        bool isSeed = false;
        for (const auto& intersection : intersections(gridView, element))
        {
            static constexpr auto eps = 1e-8; // TODO
            if (intersection.geometry().center()[dir] < lowerLeft[dir] + eps || intersection.geometry().center()[dir] > upperRight[dir] - eps)
            {
                isSeed = true;
                break;
            }
        }

        if (isSeed)
        {
            elementIsConnected[eIdx] = true;
            recursivelyFindConnectedElements(gridView, element, elementIsConnected);
        }
    }

    const auto numVoidVoxels = std::count(elementIsConnected.begin(), elementIsConnected.end(), true);
    const auto numSolidVoxels = elementIsConnected.size() - numVoidVoxels;

    std::cout << numVoidVoxels << " void voxels and " << numSolidVoxels << " solid voxels found. Porosity: "
                               << static_cast<double>(numVoidVoxels)/static_cast<double>(elementIsConnected.size()) << std::endl;

    return elementIsConnected;
}

/*!
 * \brief Assigns a cluster index for each void element.
 */
template<class GridView>
std::vector<std::size_t> clusterVoidElements(const GridView& gridView)
{
    std::vector<std::size_t> elementInCluster(gridView.size(0), 0); // initially, all elements are in pseudo cluster 0
    std::size_t clusterIdx = 0;

    for (const auto& element : elements(gridView))
    {
        const auto eIdx = gridView.indexSet().index(element);
        if (elementInCluster[eIdx]) // element already is in a cluster
            continue;

        ++clusterIdx;
        elementInCluster[eIdx] = clusterIdx;

        recursivelyFindConnectedElements(gridView, element, elementInCluster, clusterIdx);
    }

    // make sure the clusters start with index zero
    for (auto& idx : elementInCluster)
        idx -= 1;


    std::cout << "\nFound " << clusterIdx + 1 << " void clusters." << std::endl;

    return elementInCluster;
}

template<class GridView>
auto getElementsInLargestCluster(const GridView& gridView)
{
    const auto clusteredElements = clusterVoidElements(gridView);
    const auto numVoidClusters = *std::max_element(clusteredElements.begin(), clusteredElements.end()) + 1;
    std::cout << "\nFound " << numVoidClusters << " void clusters." << std::endl;

    if (getParam<bool>("Output.WriteRawVoid", false))
    {
        Dune::VTKWriter<GridView> vtkWriter(gridView);
        vtkWriter.addCellData(clusteredElements, "clusterId");
        vtkWriter.write("debug-raw-void");
    }

    // count number of elements in individual clusters
    std::vector<std::size_t> voidClusterFrequency(numVoidClusters);
    for (const auto clusterIdx : clusteredElements)
        voidClusterFrequency[clusterIdx] += 1;

    const auto largestCluster = std::max_element(voidClusterFrequency.begin(), voidClusterFrequency.end());
    const auto largestClusterIdx = std::distance(voidClusterFrequency.begin(), largestCluster);

    std::vector<bool> elementsInLargestCluster(gridView.size(0));

    for (int eIdx = 0; eIdx < clusteredElements.size(); ++eIdx)
        if (clusteredElements[eIdx] == largestClusterIdx)
            elementsInLargestCluster[eIdx] = true;

    std::cout << "Largest cluster contains " << std::count(elementsInLargestCluster.begin(), elementsInLargestCluster.end(), true) << " elements" << std::endl;

    return elementsInLargestCluster;
}

template<class GridView>
void writeGrid(const GridView& gridView, const std::string& name)
{
    static const bool vtk = getParam<bool>("Output.VTK", true);
    static const bool dgf = getParam<bool>("Output.DGF", false);
    static const bool msh = getParam<bool>("Output.MSH", true);

    if (vtk)
    {
        Dune::VTKWriter<GridView> vtkWriter(gridView);
        vtkWriter.write(name);
    }
    if (dgf)
    {
        Dune::DGFWriter<GridView> dgfWriter(gridView);
        dgfWriter.write(name + ".dgf");
    }
    if (msh)
    {
        Dune::GmshWriter<GridView> gmshWriter(gridView);
        gmshWriter.write(name + ".msh");
    }
}

template<class Container, class GridView, class GlobalPos>
void overwriteRegion(Container& rawData,
                     const GridView& gridView,
                     const GlobalPos& bBoxMin,
                     const GlobalPos& bBoxMax,
                     typename Container::value_type marker)
{
    using Box = Dune::AxisAlignedCubeGeometry<typename GlobalPos::value_type, GridView::dimension, GridView::dimension>;
    Box region(bBoxMin, bBoxMax);

    for (const auto& element : elements(gridView))
    {
        const auto center = element.geometry().center();
        if (intersectsPointGeometry(center, region))
            rawData[gridView.indexSet().index(element)] = marker;
    }
}

template<class HostGrid>
void createGridForDirection(HostGrid& hostGrid,
                            const int direction)
{
    using SubGrid = Dune::SubGrid<HostGrid::LeafGridView::dimension, HostGrid>;
    Dumux::GridManager<SubGrid> subgridManagerVoid, subgridManagerSolid;

    const std::string geomFile = getParam<std::string>("Grid.GeometryFile");
    const std::string exportName = getParam<std::string>("Output.ExportName");
    static const bool computeSolidGrid = getParam<bool>("Output.ComputeSolidGrid", false);
    static const int voidMarker = getParam<int>("Grid.VoidMarker", 0);

    using GlobalPos = typename HostGrid::template Codim<0>::Geometry::GlobalCoordinate;
    static const auto lowerLeft =  getParam<GlobalPos>("Grid.LowerLeft");
    static const auto upperRight =  getParam<GlobalPos>("Grid.UpperRight");

    static constexpr char dirToChar[] = {"xyz"};
    const auto exportdir = dirToChar[direction];
    std::cout << "Creating grid for conncted flow paths in " << exportdir << " direction" << std::endl;

    const auto& gridView = hostGrid.leafGridView();
    const auto v = readRawFileToContainer<std::vector<uint8_t>>(geomFile);
    const auto connectedElements = getPermeableVoidElements(gridView, v, lowerLeft, upperRight, direction, voidMarker);
    subgridManagerVoid.init(hostGrid, [&](const auto& element){ return connectedElements[gridView.indexSet().index(element)]; });
    writeGrid(subgridManagerVoid.grid().leafGridView(), exportName + "_void_" + exportdir);

    if (computeSolidGrid)
    {
        subgridManagerSolid.init(hostGrid, [&](const auto& element) { return !connectedElements[gridView.indexSet().index(element)]; });
        writeGrid(subgridManagerSolid.grid().leafGridView(), exportName + "_solid_" + exportdir);
    }
}
template<class HostGrid>
void createGridForLargestCluster(HostGrid& hostGrid)
{
    using SubGrid = Dune::SubGrid<HostGrid::LeafGridView::dimension, HostGrid>;
    Dumux::GridManager<SubGrid> subgridManagerVoid, subgridManagerSolid;
    std::cout << "Creating grid from largest void cluster" << std::endl;

    const std::string geomFile = getParam<std::string>("Grid.GeometryFile");
    const std::string exportName = getParam<std::string>("Output.ExportName");
    static const bool computeSolidGrid = getParam<bool>("Output.ComputeSolidGrid", false);
    static const int voidMarker = getParam<int>("Grid.VoidMarker", 0);
    const auto& gridView = hostGrid.leafGridView();
    auto v = readRawFileToContainer<std::vector<uint8_t>>(geomFile);

    const auto numPaddingRegions = getParam<std::size_t>("Grid.NumPaddingRegions", 0);
    for (int i = 0; i < numPaddingRegions; ++i)
    {
        using GlobalPos = typename HostGrid::LeafGridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;
        const auto bBoxMin = getParam<GlobalPos>("Grid.PaddingRegion" + std::to_string(i)  + ".LowerLeft");
        const auto bBoxMax = getParam<GlobalPos>("Grid.PaddingRegion" + std::to_string(i)  + ".UpperRight");
        const auto marker = getParam<int>("Grid.PaddingRegion" + std::to_string(i)  + ".Marker");
        overwriteRegion(v, gridView, bBoxMin, bBoxMax, marker);
    }

    // first, create a grid containing only void elements
    // Create the selector
    auto elementSelector = [&](const auto& element)
    {
        auto eIdx = hostGrid.leafGridView().indexSet().index(element);

        // if the hostgrid was refined, get the index of the original, un-refined
        // host grid element which fits with the image's indices
        if (element.hasFather())
        {
            auto father = element;
            while(father.hasFather())
                father = father.father();

            eIdx = hostGrid.levelGridView(father.level()).indexSet().index(father);
        }

        return v[eIdx] == voidMarker;
    };

    subgridManagerVoid.init(hostGrid, elementSelector);
    const auto& gridViewVoid = subgridManagerVoid.grid().leafGridView();

    // get subgrid indices of void elements in largest cluster
    const auto voidElementsInLargestCluster = getElementsInLargestCluster(gridViewVoid);

    // convert to hostgrid indices
    std::vector<std::size_t> subGridIdxToHostGridIdx(gridViewVoid.size(0));
    for (const auto& element : elements(gridViewVoid))
        subGridIdxToHostGridIdx[gridViewVoid.indexSet().index(element)] = gridView.indexSet().index(element.impl().hostEntity());

    std::vector<bool> connectedVoidElements(gridView.size(0), false);
    for (const auto& element : elements(gridViewVoid))
    {
        const auto subGridElementIdx = gridViewVoid.indexSet().index(element);
        const auto hostGridElementIdx = subGridIdxToHostGridIdx[subGridElementIdx];
        connectedVoidElements[hostGridElementIdx] = voidElementsInLargestCluster[subGridElementIdx];
    }

    subgridManagerVoid.init(hostGrid, [&](const auto& element){ return connectedVoidElements[gridView.indexSet().index(element)]; });

    writeGrid(subgridManagerVoid.grid().leafGridView(), exportName + "_void_largest_cluster");

    if (computeSolidGrid)
    {
        subgridManagerSolid.init(hostGrid, [&](const auto& element) { return !connectedVoidElements[gridView.indexSet().index(element)]; });
        writeGrid(subgridManagerSolid.grid().leafGridView(), exportName + "_solid_largest_cluster");
    }
}

} // end namespace Dumux


int main(int argc, char** argv)
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);

    static constexpr int dim = 3;

    Dune::Timer timer;
    std::cout << "Constructing SubGrid from raw" << std::endl;

    using HostGrid = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<double, dim>>;
    Dumux::GridManager<HostGrid> hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    if (getParam<bool>("Grid.UseLargestCluster", true))
        createGridForLargestCluster(hostGrid);
    else
    {
        const auto directions = getParam<std::vector<int>>("Output.Directions");
        for (const auto dir : directions)
            createGridForDirection(hostGrid, dir);
    }

    std::cout << "Constructing took "  << timer.elapsed() << " seconds.\n";

    return 0;
}
