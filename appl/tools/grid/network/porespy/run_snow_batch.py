import openpnm as op
import porespy as ps
import numpy as np
import argparse
import glob
import os

parser = argparse.ArgumentParser(description='extract network from image using PoreSpy')
parser.add_argument('path', type=str, help='The path to the image files')
parser.add_argument('-m','--marchingCubesArea', action='store_true', help="Use marching cubes alg. to calculate throat area")
parser.add_argument('-r', '--resolution', type=float, help='The voxel size', required=True)
parser.add_argument('-d','--dualSnow', action='store_true', help="Extract dual network")
parser.add_argument('-b','--buggyDualSnow', action='store_true', help="Use outdated buggy algorithm")
args = vars(parser.parse_args())

for filePath in glob.iglob(args['path'] + '/*.raw'):

    case = filePath.split('/')[-1].split('_')
    voxels = int(case[1])
    overlap = float(case[-1].replace('.raw', ''))

    print("Reading raw image file", filePath, "with", voxels, "voxels")
    im = np.fromfile(filePath, dtype='uint8', sep="").reshape([voxels, voxels, voxels])
    im = ~np.array(im, dtype=bool)
    im = np.swapaxes(im, 0, 2)

    print("Porosity is", ps.metrics.porosity(im))

    if args['marchingCubesArea']:
        print('Using Marching Cubes Alg. to calculate throat areas')

    #application of snow-algorithm
    if not args['dualSnow']:
        snowOutput = ps.networks.snow(im,
                        voxel_size=args['resolution'],
                        marching_cubes_area=args['marchingCubesArea'])
    else:
        if args['buggyDualSnow']:
            print("Warning: This may cause unexpected results!")
            snowOutput = ps.networks.snow_dual(im,
                    voxel_size=args['resolution'],
                    marching_cubes_area=args['marchingCubesArea'])
            # TODO: remove once fixed in PoreSpy
        else:
            snowOutput = ps.networks.snow_n(im + 1,
                    voxel_size=args['resolution'],
                    marching_cubes_area=args['marchingCubesArea'])

    # Porespy offers two possibilities to extract dual grids, yielding two different names for
    # 'void' and 'solid' pores. We harmonize this here.
    if args['dualSnow'] and not args['buggyDualSnow']:
        keys = [key for key in snowOutput]
        for key in keys:
            newKey = ''
            if 'phase1_phase2' in key:
                newKey = key.replace('phase1_phase2', 'interconnect')
            elif 'phase1' in key or 'phase2' in key:
                newKey = key.replace('phase1', 'solid').replace('phase2', 'void')
            if newKey:
                print("Renaming", key, "to", newKey)
                snowOutput[newKey] = snowOutput.pop(key)

    # use porespy to sanitize some parameters (might become obsolete as some point)
    pn, geo = op.io.PoreSpy.import_data(snowOutput)

    # trimming pore network to avoid singularity
    print('Number of pores before trimming: ', pn.Np)
    h = pn.check_network_health()
    op.topotools.trim(network=pn, pores=h['trim_pores'])
    print('Number of pores after trimming: ', pn.Np)

    filename = filePath.replace('.raw','') + ('_dual' if args['dualSnow'] else '')

    # export to vtk
    op.io.VTK.save(network=pn, filename=filename)
    print("Writing", filename + ".vtp")

    # export network for openpnm
    pn.project.save_project(filename)
    print("Writing", filename + ".pnm")
