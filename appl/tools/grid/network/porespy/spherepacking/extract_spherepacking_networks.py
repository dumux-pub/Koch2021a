import os
import openpnm as op
import numpy as np
import argparse
from openpnm.topotools import trim
import copy
import glob

def removeBoundaryThroats(net):
    pores = net.pores('boundary')
    throats = net.throats('boundary')
    cnBoundary = net["throat.conns"][throats]

    net['pore.delete'] = False
    net['pore.delete'][pores] = True

    for label in ['pore.front', 'pore.back', 'pore.bottom', 'pore.top', 'pore.left', 'pore.right']:
        for idx, throat in enumerate(cnBoundary):
            poreVals = [net[label][throat[0]], net[label][throat[1]]]
            val = np.max(poreVals) # is True if any of the two pores' label is True
            # make sure both pores get same label
            net[label][throat[0]] = val
            net[label][throat[1]] = val

    trim(network=net, throats=throats)
    trim(network=net, pores=net['pore.delete'])
    
def poreLabel(net):
    label = np.full(len(net['pore.coords']), -1, dtype=int)
    label[net['pore.left']] = 1
    label[net['pore.right']] = 2
    label[net['pore.back']] = 3
    label[net['pore.front']] = 4
    label[net['pore.bottom']] = 5
    label[net['pore.top']] = 6

    if 'pore.fake_void' in net.keys() and 'pore.fake_solid' in net.keys():
        label[net['pore.fake_void']] = 22
        label[net['pore.fake_solid']] = 33

    return label

def throatLabel(net, policy=None):
    #TODO add policy
    pLabels = poreLabel(net)
    label = np.full(len(net['throat.conns']), -1, dtype=int)

    for eIdx, element in enumerate(net['throat.conns']):
        label[eIdx] = np.max(pLabels[element]) # take the label with higher value, TODO add policy here

    if 'throat.fake_void' in net.keys() and 'throat.fake_solid' in net.keys():
        label[net['throat.fake_void']] = 22
        label[net['throat.fake_solid']] = 33

    return label

def writeDGF(filename, net, geo, policy, areaForOverWrite=None):
    
    # idea: take marching cubes area for interconnect throats and other area for intraphase throats

    vertices = net['pore.coords']
    elements = net['throat.conns']

    poreRadius = policy['pore.radius'](net, geo)
    poreInscribedRadius = policy['pore.inscribedRadius'](net, geo)
    poreExtendedRadius = policy['pore.extendedRadius'](net, geo)
    poreVolume = policy['pore.volume'](net, geo)
    poreLabel = policy['pore.label'](net, geo)

    throatRadius = policy['throat.radius'](net, geo)
    throatLength = policy['throat.length'](net, geo)
    throatArea = copy.deepcopy(policy['throat.area'](net, geo))
    throatShapeFactor = policy['throat.throatShapeFactor'](net, geo)
    throatLabel = policy['throat.label'](net, geo)
    throatCenter = geo['throat.centroid']
    
    if areaForOverWrite is not None:
        assert((throatArea[net['throat.interconnect']] != areaForOverWrite).all())
        throatArea[net['throat.interconnect']] = areaForOverWrite
            
    # create dgf data
    # 1D array (one entry for each throat) containg 0 for void, 1 for solid and 2 for interconnect
    throatDomainType = np.zeros(len(elements), dtype = np.int64)
    throatDomainType[net['throat.solid']] = 1
    throatDomainType[net['throat.interconnect']] = 2

    # 1D array (one entry for each pore) containg 0 for void, 1 for solid and 2
    poreDomainType = np.zeros(len(vertices), dtype = np.int64)
    poreDomainType[net['pore.solid']] = 1

    # porespy sets the boundary void pore centers not directly on the "real" boundary but in the center of the cut void pore bodies at the boundary.
    # We move the boundary void vertices to correct position (REV boundary) here, assuming a 3x3 solid lattice
    solidVertices = vertices[poreDomainType == 1]
    solidBBoxMin = np.array([np.min(solidVertices[:,0]), np.min(solidVertices[:,1]), np.min(solidVertices[:,2])])
    solidBBoxMax = np.array([np.max(solidVertices[:,0]), np.max(solidVertices[:,1]), np.max(solidVertices[:,2])])

    solidPoreToPoreDistance = (solidBBoxMax - solidBBoxMin)/2.0
    assert(solidPoreToPoreDistance[0] == solidPoreToPoreDistance[1] == solidPoreToPoreDistance[2])
    solidPoreToPoreDistance = solidPoreToPoreDistance[0]

    maxVoidVolume = np.max(geo['pore.region_volume'][net['pore.void'] == 1])
    print("max vol is", maxVoidVolume)

    for vIdx in range(len(vertices)):
        if poreDomainType[vIdx] == 0:
            poreVolume[vIdx] = maxVoidVolume
            for i in range(3):
                if vertices[vIdx][i] < solidBBoxMin[i]:
                    vertices[vIdx][i] = solidBBoxMin[i] - 0.5*solidPoreToPoreDistance
                if vertices[vIdx][i] > solidBBoxMax[i]:
                    vertices[vIdx][i] = solidBBoxMax[i] + 0.5*solidPoreToPoreDistance


    vdata = np.stack([vertices[:,0], vertices[:,1], vertices[:,2], poreInscribedRadius, poreExtendedRadius, poreVolume, poreLabel, poreDomainType], axis=1).reshape(len(poreLabel), 8)
    edata = np.stack([elements[:,0], elements[:,1], throatRadius, throatLength, throatArea, throatShapeFactor, throatCenter[:,0], throatCenter[:,1], throatCenter[:,2], throatLabel, throatDomainType], axis=1).reshape(len(throatLabel), 11)


    # write DGF file
    with open(filename, "w") as outputfile:
        outputfile.write("DGF\n")
        outputfile.write("% Vertex parameters: PoreInscribedRadius PoreExtendedRadius PoreVolume PoreLabel PoreDomainType\n")
        outputfile.write("% Element parameters: ThroatRadius ThroatLength ThroatCrossSectionalArea ThroatShapeFactor ThroatCenterX ThroatCenterY ThroatCenterZ ThroatLabel ThroatDomainType\n")
        outputfile.write("Vertex\n")
        outputfile.write("parameters " + str(len(vdata[0]) - 3) + "\n") # vertex data
        for i in range(len(poreLabel)):
            outputfile.write(" ".join([str(v) for v in vdata[i]]) + "\n")
        outputfile.write("\n#\n")
        outputfile.write("SIMPLEX\n")
        outputfile.write("parameters " + str(len(edata[0]) - 2) + "\n") # cell data
        for i in range(len(throatLabel)):
            outputfile.write(" ".join([str(v if idx > 1 else int(v)) for idx, v in enumerate(edata[i])]) + "\n")
        outputfile.write("\n#\n")
        outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert .pnm to .dgf')
    parser.add_argument('folderMC', type=str, help='The folder containing .pnm file witch marching cube areas')
    parser.add_argument('folderNoMC', type=str, help='The folder containing .pnm file witch marching cube areas')
    parser.add_argument('-n', '--outputname', type=str, help='The output file name', default='')
    parser.add_argument('-k','--keepBoundaryThroats', action='store_false', help="Keep the boundary throats")
    args = vars(parser.parse_args())
    
    for filepathMC in glob.iglob(args['folderMC'] + '/*.pnm'):
        filePathNoMc = filepathMC.replace(args['folderMC'] + '/', args['folderNoMC'])
        
        print('Processing', filepathMC)
    
        ws = op.Workspace()
        ws.clear()
        projectMC = ws.load_project(filepathMC)
        projectNoMC = ws.load_project(filePathNoMc)

        netMC, geoMC = projectMC
        netNoMC, geoNoMC= projectNoMC
        
        removeBoundaryThroats(netMC)
        removeBoundaryThroats(netNoMC)
        
        assert('pore.solid' in netMC.keys() and 'pore.solid' in netNoMC.keys())


        ##### begin policy ###############################################################################
        # TODO: how can we conveniently load / exchange this?
        def throatLength(n, g):
            elements = n['throat.conns']
            length = copy.deepcopy(g['throat.direct_length'])
            poreRadius = g['pore.inscribed_diameter'] /2.0

            for eIdx, element in enumerate(elements):
                vIdx0, vIdx1 = element[0], element[1]
                length[eIdx] = length[eIdx] - poreRadius[vIdx0] - poreRadius[vIdx1]

            return np.clip(length, 1e-9, np.max(length))

        def throatShapeFactor(n, g):
            G = g['throat.area'] / g['throat.perimeter']**2
            maxG = 1.0/16.0 # for circle
            return np.clip(G, 0.0, maxG)

        policy = {'pore.radius': lambda n, g : g['pore.extended_diameter'] / 2.0,
                'pore.inscribedRadius': lambda n, g : g['pore.inscribed_diameter'] / 2.0,
                'pore.extendedRadius': lambda n, g : g['pore.extended_diameter'] / 2.0,
                'pore.volume': lambda n, g : g['pore.region_volume'],
                'pore.label': lambda n, g : poreLabel(n),
                'throat.radius': lambda n, g : g['throat.inscribed_diameter'] / 2.0,
                'throat.length': throatLength,
                'throat.area': lambda n, g : g['throat.area'],
                'throat.throatShapeFactor': throatShapeFactor,
                'throat.label': throatLabel}
        ##### end policy ################################################################################

        if not args['outputname']:
            filename = os.path.splitext(filepathMC)[0] + "_sanitized.dgf"
        else:
            filename = os.path.splitext(args["outputname"])[0] + ".dgf"
            
        print('filename is', filename)

        print("Writing", filename)
        
        # write with MC
        writeDGF(filename=os.path.splitext(filepathMC)[0] + "_mc.dgf" , net=netMC, geo=geoMC, policy=policy)
        
        # write without MC
        writeDGF(filename=os.path.splitext(filepathMC)[0] + "_no_mc.dgf" , net=netNoMC, geo=geoNoMC, policy=policy)
        
        assert((geoMC['throat.area'][netNoMC['throat.interconnect']] != geoNoMC['throat.area'][netNoMC['throat.interconnect']]).all())
        
        writeDGF(filename=os.path.splitext(filepathMC)[0] + "_no_mc_intra_mc_inter.dgf" , net=netNoMC, geo=geoNoMC, policy=policy, areaForOverWrite=geoMC['throat.area'][netNoMC['throat.interconnect']])
        
        writeDGF(filename=os.path.splitext(filepathMC)[0] + "_mc_intra_no_mc_inter.dgf" , net=netMC, geo=geoMC, policy=policy, areaForOverWrite=geoNoMC['throat.area'][netMC['throat.interconnect']])

        ##export to vtk
        #op.io.VTK.save(network=net, filename=filename)
        #print("Writing", filename + ".vtp")
