import scipy as sp
import numpy as np
import porespy as ps
import matplotlib.pyplot as plt
ps.visualization.set_mpl_style()
sp.random.seed(10)

im = ps.generators.lattice_spheres(shape=[500, 500, 500], radius=45, offset=-1)
im = im[1:442, 1:442, 1:442]

fig, ax = plt.subplots(3, 2, figsize=[10,10])

ax[0][0].imshow(im[0,:,:])
ax[0][1].imshow(im[-1,:,:])

ax[1][0].imshow(im[:,0,:])
ax[1][1].imshow(im[:,-1,:])

ax[2][0].imshow(im[:,:,0])
ax[2][1].imshow(im[:,:,-1])

im = ~im

print(im.shape)

plt.show()

im.tofile("test_3d.raw")
