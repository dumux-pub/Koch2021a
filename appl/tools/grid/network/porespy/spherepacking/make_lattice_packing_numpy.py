import numpy as np

radius = 50
overlap = 2
num_spheres_per_direction = 3

print("pixel overlap: ", overlap)
ovl_fraction = 1.0 - overlap/radius
print("overlap fraction: ", ovl_fraction)

dx = radius - overlap
twodx = 2*dx
size = num_spheres_per_direction*twodx
print("size: {0}x{0}x{0}".format(size))

def compute_flag(i, j, k):
    posi, posj, posk = (i % twodx) - dx + 0.5, (j % twodx) - dx + 0.5, (k % twodx) - dx + 0.5
    if posi**2 + posj**2 + posk**2 < radius**2:
        return 1
    else:
        return 0

img = np.zeros(shape=(size, size, size), dtype=np.uint8)
for i in range(0, size):
    for j in range(0, size):
        for k in range(0, size):
            img[i][j][k] = compute_flag(i, j, k)

unique, counts = np.unique(img, return_counts=True)
res = dict(zip(unique, counts))
print(res)
print("porosity", res[0]/size**3)
print("solidity", res[1]/size**3)

img.tofile("{}_{}_{:.3f}.raw".format(num_spheres_per_direction, size, ovl_fraction))

import matplotlib.pyplot as plt
plt.imshow(img[:,:,radius])
plt.colorbar()
plt.plot()
plt.show()
