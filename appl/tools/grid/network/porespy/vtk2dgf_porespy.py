#! /usr/bin/env python

"""
Convert a vtp file to a dgf file
Script for specific openpnm vtp file to dgf readable by the dumux porenetwork module
"""
import argparse
import xml.etree.ElementTree as ET
import sys
import os
import numpy as np
import re

def sanitize(tree, removeBoundarThroats = False):

    root = tree.getroot()

    # remove vertices not connected to any element
    # get coordinates
    for dataArray in root.findall(".//Points/DataArray"):
        if dataArray.attrib["Name"] == "coords":
            coords1D = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)
            coords = coords1D.reshape(len(coords1D)//3, 3)

    # this only considers pores that are actually connected to throats
    for dataArray in root.findall(".//Lines/DataArray"):
        if dataArray.attrib["Name"] == "connectivity":
            connectivity = np.fromstring(dataArray.text, sep=" ", dtype=np.int64)

            if removeBoundarThroats:
                for dataArray in root.findall(".//CellData/DataArray"):
                    if 'throat.boundary' in dataArray.attrib["Name"]:
                        boundaryThroats = np.fromstring(dataArray.text, sep=" ", dtype=bool)
                        elements = connectivity.reshape(len(connectivity)//2, 2)
                        before = len(elements)
                        elements = np.delete(elements, boundaryThroats, axis=0)
                        print("Deleted", before-len(elements), "boundary throats")
                        connectivity = elements.ravel()

            newToOldVertexIndices = np.unique(connectivity)
            oldToNewVertexIndices = {}
            for i, ii in enumerate(newToOldVertexIndices):
                oldToNewVertexIndices[ii] = i

    newCoords = coords[newToOldVertexIndices]
    newConnectivity = np.array([oldToNewVertexIndices[i] for i in connectivity])

    # sanitize points
    for dataArray in root.findall(".//Points/DataArray"):
        if dataArray.attrib["Name"] == "coords":
            dataArray.attrib["RangeMin"] = str(np.min(newCoords))
            dataArray.attrib["RangeMax"] = str(np.max(newCoords))
            dataArray.text = "\n" + re.sub('[\[\]]', '', np.array2string(newCoords, max_line_width=100, precision=15, suppress_small=True, threshold=1e9)) + "\n"

    # sanitize lines
    for dataArray in root.findall(".//Lines/DataArray"):
        if dataArray.attrib["Name"] == "connectivity":
            dataArray.attrib["RangeMin"] = str(np.min(newConnectivity))
            dataArray.attrib["RangeMax"] = str(np.max(newConnectivity))
            dataArray.text = "\n" + re.sub('[\[\]]', '', np.array2string(newConnectivity, max_line_width=100, threshold=1e9)) + "\n"

    # sanitize point data
    for dataArray in root.findall(".//PointData/DataArray"):
        type = np.int64 if dataArray.attrib["type"] == "Int64" else np.float64
        newDataArray = np.fromstring(dataArray.text, sep=" ", dtype=type)[newToOldVertexIndices]
        dataArray.attrib["RangeMin"] = str(np.min(newDataArray))
        dataArray.attrib["RangeMax"] = str(np.max(newDataArray))
        dataArray.text = "\n" + re.sub('[\[\]]', '', np.array2string(newDataArray, max_line_width=100, threshold=1e9)) + "\n"

    # sanitize cell data
    if removeBoundarThroats:
        for dataArray in root.findall(".//CellData/DataArray"):
            type = np.int64 if dataArray.attrib["type"] == "Int64" else np.float64
            newDataArray = np.fromstring(dataArray.text, sep=" ", dtype=type)
            newDataArray = np.delete(newDataArray, boundaryThroats)
            dataArray.attrib["RangeMin"] = str(np.min(newDataArray))
            dataArray.attrib["RangeMax"] = str(np.max(newDataArray))
            dataArray.text = "\n" + re.sub('[\[\]]', '', np.array2string(newDataArray, max_line_width=100, threshold=1e9)) + "\n"

    # sanitize meta data
    for dataArray in root.findall(".//Piece"):
        dataArray.attrib["NumberOfPoints"] = str(len(newCoords))

def getDualGridCoordinationNumber(poreData, elements):
    numPoresTotal = len(poreData['label'])
    poreData['coordNumVoid'] = poreData['coordNumSolid'] = np.zeros(numPoresTotal, dtype=np.int64)
    for vIdx0, vIdx1 in elements:
        if poreData['domainType'][vIdx0] == 0 and poreData['domainType'][vIdx1] == 0:
            poreData['coordNumVoid'][vIdx0] += 1
            poreData['coordNumVoid'][vIdx1] += 1
        elif poreData['domainType'][vIdx0] == 1 and poreData['domainType'][vIdx1] == 1:
            poreData['coordNumSolid'][vIdx0] += 1
            poreData['coordNumSolid'][vIdx1] += 1



def sanitizeDualNetwork(vertices, poreData, elements, throatData):
    numPoresTotal = len(vertices)

    # calculate coordination number for void-void and solid-solid connections
    getDualGridCoordinationNumber(poreData, elements)

    for domain in ['void', 'solid']:
        if domain == 'void':
            coordinationNumber = poreData['coordNumVoid']
            label = 22
            domainType = 0
        else:
            coordinationNumber = poreData['coordNumSolid']
            label = 33
            domainType = 1

        newPoreIdx = numPoresTotal

        # split void pores only connect to solid pores and add an artifical throat
        for vIdx, coordNum in enumerate(coordinationNumber):
            if poreData['domainType'][vIdx] == domainType and coordNum < 1:
                if domain == 'void':
                    print("Void pore", vIdx, "is only connected to solid pores")
                else:
                    print("Solid pore", vIdx, "is only connected to void pores")
                vertices = np.vstack((vertices, vertices[vIdx] + 1e-10)) # clone the vertex and shift its coordinates a bit

                for key in poreData:
                    if 'diameter' in key or 'volume' in key:
                        poreData[key] = np.append(poreData[key], 1e-20*poreData[key][vIdx]) # use a very small radius/volume

                poreData['label'] = np.append(poreData['label'], label) # add a unique label
                poreData['domainType'] = np.append(poreData['domainType'], domainType)

                print("Adding new elem no. ", len(elements))
                elements = np.vstack((elements, np.array([vIdx, newPoreIdx])))

                for key in throatData:
                    if 'diameter' in key or 'area' in key:
                        throatData[key] = np.append(throatData[key], 1e10) # use a very large diamter/area
                    if 'length' in key:
                        throatData[key] = np.append(throatData[key], 1e-20) # use a very small length

                throatData['throatShapeFactor'] = np.append(throatData['throatShapeFactor'], 1.0/16.0) # arbitrary
                throatData['label'] = np.append(throatData['label'], -1)
                throatData['domainType'] = np.append(throatData['domainType'], domainType)
                newPoreIdx += 1

    # check success
    getDualGridCoordinationNumber(poreData, elements)
    print("Min coord num void:", np.min(poreData['coordNumVoid']))
    print("Min coord num solid:", np.min(poreData['coordNumSolid']))

    return vertices, poreData, elements, throatData


if __name__ == "__main__":
    # read the command line arguments
    parser = argparse.ArgumentParser(description='vtp to dgf converter')
    parser.add_argument('vtpfile', type=str, help='The vtp file to convert')
    parser.add_argument('--scaling', type=float, default=1.0, help='The scaling factor')
    parser.add_argument('--minThroatLength', type=float, default=1e-9, help='The minimum throat length')
    parser.add_argument('--minThroatPerimeter', type=float, default=0.0, help='The minimum throat perimeter')
    parser.add_argument('--2D', type=bool, default=False, help='Enable 2D grids')
    parser.add_argument('--usePoreSpyPoreLabels', type=bool, default=False, help='Use the pore labels provided by poreSpy')
    parser.add_argument('--removeBoundaryThroats', type=bool, default=False, help='Remove the boundary labels provided by poreSpy')
    parser.add_argument('--throatLength', type=str, default='direct_length', help='Allows to choose different length types')
    args = vars(parser.parse_args())

    # construct element tree from vtk file
    tree = ET.parse(args["vtpfile"])
    sanitize(tree, args["removeBoundaryThroats"])
    root = tree.getroot()
    piece = root[0][0]

    # get vertices and elements
    vertices = np.fromstring(piece.find("Points/DataArray[@Name='coords']").text, sep=" ", dtype=np.float64)
    vertices = vertices.reshape(len(vertices)//3, 3)
    connectivity = np.fromstring(piece.find("Lines/DataArray[@Name='connectivity']").text, sep=" ", dtype=np.int64)
    elements = connectivity.reshape(len(connectivity)//2, 2)

    poreSpyPoreLabels = {}
    poreLabelNames = ["left", "right", "bottom", "top", "back", "front"]
    poreLabel = np.zeros(len(vertices), dtype=int)
    poreSpyToDumuxLabelMap = {"left":1, "right":2, "bottom":3, "top":4, "back":5, "front":6}

    vtkToPythonDataType = {'Float64':np.float64, 'Int64':np.int64}
    isDualNetwork = False
    poreData = {}
    throatData = {}

    # get point data (pore radius, pore label)
    for dataArray in root.findall(".//PointData/DataArray"):
        name = dataArray.attrib["Name"].split('|')[-1].replace(' pore.', '')
        # Porespy offers two possibilities to extract dual grids, yielding two different names for
        # 'void' and 'solid' pores. We harmonize this here.
        name = name.replace('phase1', 'solid')
        name = name.replace('phase2', 'void')
        poreData[name] = np.fromstring(dataArray.text, sep=" ", dtype=vtkToPythonDataType[dataArray.attrib["type"]])
        if 'solid' in name or 'void' in name:
            isDualNetwork = True

        # get pore labels
        if args["usePoreSpyPoreLabels"]:
            for label in ["left", "right", "bottom", "top", "back", "front"]:
                if ("pore." + label) in dataArray.attrib["Name"]:
                    poreData['label'][np.fromstring(dataArray.text, sep=" ", dtype=bool)] = poreSpyToDumuxLabelMap[label]
                    break

    assert(len(vertices) == len(poreData['label']))
    numPoresTotal = len(vertices)
    print("Found", numPoresTotal, "pores in total")
    poreData['vIdx'] = poreData['label']
    poreData['label'].fill(0)

    if isDualNetwork:
        print("Processing dual network")
        numVoidPores = np.sum(poreData['void'])
        numSolidPores = np.sum(poreData['solid'])
        print("Found", numVoidPores, "void pores")
        print("Found", numSolidPores, "solid pores")
        # NOTE: boundary pores are neither void nor solid

    # get cell data (throat radius, throat length, throat label)
    for dataArray in root.findall(".//CellData/DataArray"):
        name = dataArray.attrib["Name"].split('|')[-1].replace(' throat.', '')
        # Porespy offers two possibilities to extract dual grids, yielding two different names for
        # 'void' and 'solid' pores. We harmonize this here.
        name = name.replace('phase1_phase2', 'interconnect')
        name = name.replace('phase1', 'solid')
        name = name.replace('phase2', 'void')
        throatData[name] = np.fromstring(dataArray.text, sep=" ", dtype=vtkToPythonDataType[dataArray.attrib["type"]])

    throatData['label'] = np.zeros(len(elements), dtype = np.int64)

    # scale data
    spatialScale = args["scaling"]
    if spatialScale != 1.0:
        print("Scaling dimensions with", spatialScale)
        vertices = vertices*spatialScale

        def doScaling(data):
            for key in data:
                if any(prop in key for prop in ['length', 'diameter']):
                    data[key] *= spatialScale
                if 'area' in key:
                    data[key] *= spatialScale**2
                if 'volume' in key:
                    data[key] *= spatialScale**3

        doScaling(poreData)
        doScaling(throatData)


    # sanitize some throat properties
    for key in throatData:
        if 'length' in key:
            throatData[key] = np.clip(throatData[key], args['minThroatLength'], np.max(throatData[key]))
        if (args['minThroatPerimeter'] > 0.0) and 'perimeter' in key:
            throatData[key] = np.clip(throatData[key], args['minThroatPerimeter'], np.max(throatData[key]))

    throatData['throatShapeFactor'] = throatData['area'] / throatData['perimeter']**2

    if (isDualNetwork):
        # 1D array (one entry for each throat) containg 0 for void, 1 for solid and 2 for interconnect
        throatData['domainType'] = np.zeros(len(throatData['interconnect']), dtype = np.int64)
        throatData['domainType'][throatData['solid']] = 1
        throatData['domainType'][throatData['interconnect']] = 2

        # 1D array (one entry for each pore) containg 0 for void, 1 for solid and 2
        poreData['domainType'] = np.zeros(numPoresTotal, dtype = np.int64)
        poreData['domainType'][poreData['solid']] = 1


    # create boundary pore and throat labels
    bBox = np.array([[np.min(vertices[:,0]), np.min(vertices[:,1]), np.min(vertices[:,2])],
                     [np.max(vertices[:,0]), np.max(vertices[:,1]), np.max(vertices[:,2])]])

    eps = 1e-2 * (bBox[1][0]- bBox[0][0])

    leftVertices = np.where(vertices[:,0] < bBox[0][0] + eps)
    rightVertices = np.where(vertices[:,0] > bBox[1][0] - eps)

    frontVertices = np.where(vertices[:,1] < bBox[0][1] + eps)
    backVertices = np.where(vertices[:,1] > bBox[1][1] - eps)

    if (not args["2D"]):
        bottomVertices = np.where(vertices[:,2] < bBox[0][2] + eps)
        topVertices = np.where(vertices[:,2] > bBox[1][2] - eps)

    # set pore labels for inflow and outflow
    if not args["usePoreSpyPoreLabels"]:
        poreData['label'][leftVertices] = 1
        poreData['label'][rightVertices] = 2
        poreData['label'][frontVertices] = 3
        poreData['label'][backVertices] = 4

        if (not args["2D"]):
            poreData['label'][bottomVertices] = 5
            poreData['label'][topVertices] = 6

    # compute throat labels
    for i in range(len(elements)):
        if elements[i,0] in leftVertices[0] or elements[i,1] in leftVertices[0]:
            throatData['label'][i] = 1
        if elements[i,0] in rightVertices[0] or elements[i,1] in rightVertices[0]:
            throatData['label'][i] = 2
        if elements[i,0] in frontVertices[0] or elements[i,1] in frontVertices[0]:
            throatData['label'][i] = 3
        if elements[i,0] in backVertices[0] or elements[i,1] in backVertices[0]:
            throatData['label'][i] = 4

        if (not args["2D"]):
            if elements[i,0] in bottomVertices[0] or elements[i,1] in bottomVertices[0]:
                throatData['label'][i] = 5
            if elements[i,0] in topVertices[0] or elements[i,1] in topVertices[0]:
                throatData['label'][i] = 6

    # add artifical pores and throats for singular pores that are only connected to the other phase
    if isDualNetwork:
        # this is ugly - is there a nicer way?
        vertices, poreData, elements, throatData = sanitizeDualNetwork(vertices, poreData, elements, throatData)


    # TODO create policy helper
    poreRadius = poreData['extended_diameter']/2.0
    poreVolume = poreData['region_volume']
    poreLabel = poreData['label']
    throatRadius = throatData['inscribed_diameter']/2.0
    throatArea = throatData['area']
    throatShapeFactor = throatData['throatShapeFactor']
    throatLabel = throatData['label']

    throatLength = throatData['direct_length']
    for eIdx, element in enumerate(elements):
        vIdx0, vIdx1 = element[0], element[1]
        throatLength[eIdx] = throatLength[eIdx] - poreRadius[vIdx0] - poreRadius[vIdx1]

    # avoid negative throat lengths
    throatLength = np.clip(throatLength, args['minThroatLength'], np.max(throatLength))

    ### end policy helper

    # create dgf data
    if isDualNetwork:
        vdata = np.stack([vertices[:,0], vertices[:,1], vertices[:,2], poreRadius, poreVolume, poreLabel, poreData['domainType']], axis=1).reshape(len(poreLabel), 7)
        edata = np.stack([elements[:,0], elements[:,1], throatRadius, throatLength, throatArea, throatShapeFactor, throatLabel, throatData['domainType']], axis=1).reshape(len(throatLabel), 8)
    else:
        vdata = np.stack([vertices[:,0], vertices[:,1], vertices[:,2], poreRadius, poreVolume, poreLabel], axis=1).reshape(len(poreLabel), 6)
        edata = np.stack([elements[:,0], elements[:,1], throatRadius, throatLength, throatArea, throatShapeFactor, throatLabel], axis=1).reshape(len(throatLabel), 7)

    # write DGF file
    with open(os.path.splitext(args["vtpfile"])[0] + ".dgf", "w") as outputfile:
        outputfile.write("DGF\n")
        outputfile.write("% Vertex parameters: PoreRadius PoreVolume PoreLabel" + (" PoreDomainType" if isDualNetwork else "") +"\n")
        outputfile.write("% Element parameters: ThroatRadius ThroatLength ThroatCrossSectionalArea ThroatShapeFactor ThroatLabel" + (" ThroatDomainType" if isDualNetwork else "") + "\n")
        outputfile.write("Vertex\n")
        outputfile.write("parameters " + str(len(vdata[0]) - 3) + "\n") # vertex data
        for i in range(len(poreLabel)):
            outputfile.write(" ".join([str(v) for v in vdata[i]]) + "\n")
        outputfile.write("\n#\n")
        outputfile.write("SIMPLEX\n")
        outputfile.write("parameters " + str(len(edata[0]) - 2) + "\n") # cell data
        for i in range(len(throatLabel)):
            outputfile.write(" ".join([str(v if idx > 1 else int(v)) for idx, v in enumerate(edata[i])]) + "\n")
        outputfile.write("\n#\n")
        outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")
