import numpy as np
import openpnm as op
from openpnm.topotools import trim

resolution = 5.345e-6
voxels = [200,200,200]

# create workspace instance
ws = op.Workspace();
ws.clear();
print(ws.keys());

project = ws.load_project(filename="../grids/Berea_center_cropped_200_200.pnm")

print(ws.keys());
print(project)
net = project['net_01']
geo = project['geo_01']

pn = net

# uncomment to cut off boundary pores
Ps = net.pores('boundary')
Ts = net.throats('boundary')
cnBoundary = net["throat.conns"][Ts]

net['pore.delete'] = False
net['pore.delete'][Ps] = True

for label in ['pore.front', 'pore.back', 'pore.bottom', 'pore.top', 'pore.left', 'pore.right']:
    for idx, throat in enumerate(cnBoundary):
        poreVals = [net[label][throat[0]], net[label][throat[1]]]
        val = np.max(poreVals) # is True if any of the two pores' label is True
        # make sure both pores get same label
        net[label][throat[0]] = val
        net[label][throat[1]] = val

trim(network=net, throats=Ts)
trim(network=net, pores=net['pore.delete'])

net['pore.boundary_label'] = -1
net['pore.boundary_label'][net['pore.left']] = 1
net['pore.boundary_label'][net['pore.right']] = 2
net['pore.boundary_label'][net['pore.back']] = 3
net['pore.boundary_label'][net['pore.front']] = 4
net['pore.boundary_label'][net['pore.bottom']] = 5
net['pore.boundary_label'][net['pore.top']] = 6

air = op.phases.Air(network=pn)
phys_air = op.physics.Standard(network=pn, phase=air, geometry=geo)

A = geo['throat.area']
R_eq_square = np.sqrt(A/4.0)
R_eq_cicle = np.sqrt(A/np.pi)
R_eq_ps = geo['throat.equivalent_diameter']/2.0

G = {"circle": 1.0/(4.0*np.pi), "square":1.0/16.0 , "triangle":np.sqrt(3.0)/36.}
k = {"circle":0.5, "square":0.5623, "triangle":0.6}

# sanitize perimeter
#geo['throat.perimeter'] = np.clip(geo['throat.perimeter'], 4*resolution, 1)

hydRad = 2.0*A / geo['throat.perimeter']
shapeFactorT = A / geo['throat.perimeter']**2

maxG = 1.0/16.0 # for circle
shapeFactorT = np.clip(shapeFactorT, 0.0, maxG)

throats = geo.map_throats(throats=pn.Ts, origin=pn)
cn = geo["throat.conns"][throats]

#r1 = geo["pore.inscribed_diameter"][cn[:, 0]] * 0.5
#r2 = geo["pore.inscribed_diameter"][cn[:, 1]] * 0.5
r1 = geo["pore.extended_diameter"][cn[:, 0]] * 0.5
r2 = geo["pore.extended_diameter"][cn[:, 1]] * 0.5

L = geo['throat.direct_length'] - r1 -r2

#L = np.clip(L, 1e-19, 1)
L = np.clip(L, 1e-9, 1)

def getK(shapeFactor):
    if shapeFactor <= G["triangle"]:
        return 0.6
    elif shapeFactor <= G["square"]:
        return 0.5623
    else:
        return 0.5


k = np.array([getK(x) for x in shapeFactorT])
phys_air['throat.hydraulic_conductance'] = k * A**2 * shapeFactorT / (air['pore.viscosity'].max()*L)
#phys_air['throat.hydraulic_conductance'] = (np.pi*(R**4))/(8*air['pore.viscosity'].max()*L)
#phys_air['throat.hydraulic_conductance'] = ((hydRad**2) * A)/(8*air['pore.viscosity'].max()*L)
#phys_air['throat.hydraulic_conductance'] = ((R**2) * A)/(8*air['pore.viscosity'].max()*L)
##phys_air['throat.hydraulic_conductance'] = (np.pi*(R**4))/(8*air['pore.viscosity'].max()*L)


#for i in range(len(k)):
    #print("G:", shapeFactorT[i], "k", k[i], "A", A[i], "L", L[i], "trans", phys_air['throat.hydraulic_conductance'][i]*air['pore.viscosity'].max())

directions = {'x':['right','left'], 'y':['front','back'], 'z':['top','bottom']}
dirToNumber = {'x':[2,1], 'y':[4,3], 'z':[6,5]}

for dir in ['x','y','z']:
    perm = op.algorithms.StokesFlow(network=pn)
    perm.setup(phase=air)
    outletpores = np.nonzero(net['pore.boundary_label'] == dirToNumber[dir][0])
    inletpores = np.nonzero(net['pore.boundary_label'] == dirToNumber[dir][1])
    perm.set_value_BC(pores=outletpores, values=0)
    perm.set_value_BC(pores=inletpores, values=0.0105614)
    #perm.set_value_BC(pores=pn.pores(directions[dir][0]), values=0)
    #perm.set_value_BC(pores=pn.pores(directions[dir][1]), values=0.0105614)
    perm.run()
    air.update(perm.results())

    Q = perm.rate(pores=inletpores, mode='group')[0]
    #Q = perm.rate(pores=pn.pores(directions[dir][1]), mode='group')[0]

    A = (voxels[0] * voxels[1]) * resolution**2
    L = voxels[2] * resolution
    mu = air['pore.viscosity'].max()
    delta_P = 0.0105614 - 0
    K = Q * L * mu / (A * delta_P)
    print(dir + "-direction")
    print(f'The value of K is: {K/0.98e-12:.2f} D')
    print('The value of K is:', K)

    op.io.VTK.save(network=pn, phases=air, filename='resut_' + dir + '_direction')

    # Perform StokesFlow algorithm to calculate permeability
    airNew = op.phases.Air(network=net)
    phys_airNew = op.physics.Standard(network=net, phase=airNew, geometry=geo)
    permNew = op.algorithms.StokesFlow(network=net)
    permNew.setup(phase=airNew)
    permNew.set_value_BC(pores=outletpores, values=0)
    permNew.set_value_BC(pores=inletpores, values=0.0105614)
    permNew.run()
    airNew.update(permNew.results())

    # Original image shape (tomography data) needed to calculate cross-sectional area and length
    shape = np.array(voxels)
    voxel_size = resolution
    area = shape[[0, 2]].prod() * voxel_size**2
    length = shape[1] * voxel_size
    K = permNew.calc_effective_permeability(domain_area=area, domain_length=length)
    # 1e12 is the conversion factor to convert from SI to darcy units
    print(f"Permeability openPNM func: {K[0]/0.98e-12:.4f} Darcy")
