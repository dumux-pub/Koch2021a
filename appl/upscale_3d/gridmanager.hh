// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup InputOutput
 */
#ifndef DUMUX_IO_IMAGE_GRID_MANAGER_HH
#define DUMUX_IO_IMAGE_GRID_MANAGER_HH

#include <dumux/io/grid/gridmanager_yasp.hh>

namespace Dumux {

/*!
 * \ingroup InputOutput
 * \brief Provides a grid manager for SubGrids
 *        from information in the input file
 */
class ImageGridManager
{
public:
    using Grid = Dune::YaspGrid<3>;
    using Image = double;

    /*!
     * \brief Make the subgrid without host grid and element selector
     * This means we try to construct the element selector from the input file
     * \param paramGroup the parameter file group to check
     */
    void init(const std::string& paramGroup = "")
    {
        // check if there is an image file we can construct the element selector from
        if (hasParamInGroup(paramGroup, "Grid.Image"))
        {
            const auto rawFileName = getParamFromGroup<std::string>(paramGroup, "Grid.Image");
            imageData_ = readRawFileToContainer(rawFileName);
            const auto size = int(std::cbrt(imageData_.size()));
            std::array<int, 3> cells; cells.fill(size);
            using GlobalPosition = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
            const auto upperRight = getParamFromGroup<GlobalPosition>(paramGroup, "Grid.UpperRight");
            yasp_.init(upperRight, cells);

            // we would need to communicate the data
            if (grid().comm().size() > 1)
                DUNE_THROW(Dune::NotImplemented, "Raw image reader does not work in parallel yet");
        }
        else
            yasp_.init();
    }

    Grid& grid()
    { return yasp_.grid(); }

    const std::vector<char>& data() const
    { return imageData_; }

private:
    std::vector<char> readRawFileToContainer(const std::string& path)
    {
        std::cout << "Reading " << path << std::endl;
        // open the file:
        std::ifstream file(path, std::ios::binary);
        // skip new lines in binary mode:
        file.unsetf(std::ios::skipws);
        // get its size:
        file.seekg(0, std::ios::end);
        const std::size_t fileSize = file.tellg();
        file.seekg(0, std::ios::beg);
        // read the data:
        std::vector<char> img(fileSize);
        file.read(img.data(), fileSize);
        std::cout << img.size() << " voxels found." << std::endl;
        return img;
    }

    std::vector<char> imageData_;
    GridManager<Grid> yasp_;
};

} // end namespace Dumux

#endif
