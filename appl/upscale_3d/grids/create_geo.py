#!/usr/bin/env python3

import numpy as np
import itertools

R = 1.0
f = 0.9

dx = f*R
num_spheres_per_direction = 1
length = 2*num_spheres_per_direction*dx
num_spheres = num_spheres_per_direction**3

def sphere(pos, radius, index):
    return "Sphere({}) = {{{}, {}, {}, {}, -Pi/2, Pi/2, 2*Pi}};\n".format(index, pos[0], pos[1], pos[2], radius)

with open("spheres_void.geo", "w") as geofile:
    geofile.write("SetFactory(\"OpenCASCADE\");\n")
    geofile.write("Box(1) = {{{0}, {0}, {0}, {1}, {1}, {1}}};\n".format(0.0, length))

    posx = np.linspace(dx, length-dx, num_spheres_per_direction, endpoint=True)
    for index, pos in enumerate(itertools.product(posx, posx, posx)):
        geofile.write(sphere(pos=pos, radius=R, index=index+2))

    tools = "".join("Volume{{{}}}; ".format(index+2) for index in range(num_spheres))
    geofile.write("BooleanDifference{{ Volume{{1}}; Delete; }}{{ {} Delete; }}\n".format(tools))

with open("spheres_solid.geo", "w") as geofile:
    geofile.write("SetFactory(\"OpenCASCADE\");\n")

    posx = np.linspace(dx, length-dx, num_spheres_per_direction, endpoint=True)
    for index, pos in enumerate(itertools.product(posx, posx, posx)):
        geofile.write(sphere(pos=pos, radius=R, index=index+1))

    tools = "".join("Volume{{{}}}; ".format(index+1) for index in range(1, num_spheres))
    geofile.write("BooleanUnion{{ Volume{{1}}; Delete; }}{{ {} Delete; }}\n".format(tools))

    geofile.write("Box(2) = {{{0}, {0}, {0}, {1}, {1}, {1}}};\n".format(0.0, length))
    geofile.write("BooleanIntersection{ Volume{1}; Delete; }{ Volume{2}; Delete; }\n")
