// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TRACER_TEST_PROBLEM_HH
#define DUMUX_TRACER_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

template <class TypeTag>
class TracerTest : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    //! property that defines whether mole or mass fractions are used
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    TracerTest(std::shared_ptr<const GridGeometry> gridGeometry, const std::vector<char>& marker)
    : ParentType(gridGeometry), marker_(marker)
    {
        direction_ = getParam<int>("Problem.Direction");

        const auto& bBoxMin = this->gridGeometry().bBoxMin();
        const auto& bBoxMax = this->gridGeometry().bBoxMax();
        length_ = bBoxMax[direction_] - bBoxMin[direction_];
        offset_ = bBoxMin[direction_];
        eps_ = 1e-6*length_;
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        const auto& bBoxMin = this->gridGeometry().bBoxMin();
        const auto& bBoxMax = this->gridGeometry().bBoxMax();

        if (globalPos[direction_] > bBoxMax[direction_] - eps_ || globalPos[direction_] < bBoxMin[direction_] + eps_)
            values.setAllDirichlet();

        return values;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        return initialAtPos(globalPos);
    }

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        return PrimaryVariables((globalPos[direction_] - offset_)/length_);
    }

    bool hasMarkers() const
    { return !marker_.empty(); }

    bool marker(std::size_t eIdx) const
    { return marker_[eIdx] == 0; }

private:
    const std::vector<char>& marker_;
    int direction_;
    Scalar length_;
    Scalar offset_;
    Scalar eps_;
};

} // end namespace Dumux

#endif
