// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/pdesolver.hh>
#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/format.hh>

#include "gridmanager.hh"
#include "properties.hh"

int main(int argc, char** argv) try
{
    using namespace Dumux;

    //! define the type tag for this problem
    using TypeTag = Properties::TTag::TracerTest;

    //! initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // parse the command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    ImageGridManager gridManager;
    gridManager.init();
    const auto& gridData = gridManager.data();

    //! we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    //! create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();

    //! the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry, gridData);

    //! the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x;
    problem->applyInitialSolution(x);

    //! the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    //! intialize the vtk output module
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    vtkWriter.addVolumeVariable([](const auto& v){ return v.moleFraction(0, 0)*v.molarDensity(0); }, "T");
    vtkWriter.addVolumeVariable([](const auto& v){ return v.diffusionCoefficient(0, 0, 0); }, "λ");
    vtkWriter.write(0.0, Dune::VTK::appendedraw);

    //! the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    //! the linear solver
    using LinearSolver = AMGBiCGSTABBackend<LinearSolverTraits<GridGeometry>>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, gridGeometry->dofMapper());

    LinearPDESolver<Assembler, LinearSolver> solver(assembler, linearSolver);
    solver.solve(x);

    // write output
    vtkWriter.write(1.0, Dune::VTK::appendedraw);

    //! Compute fluxes over the Dirichlet boundaries
    const int direction = getParam<int>("Problem.Direction");
    const auto& bBoxMin = gridGeometry->bBoxMin();
    const auto& bBoxMax = gridGeometry->bBoxMax();
    const auto length = bBoxMax[direction] - bBoxMin[direction];
    const auto eps = length*1e-6;

    using LocalResidual = GetPropType<TypeTag, Properties::LocalResidual>;
    LocalResidual localResidual(&*problem, nullptr);

    // flux calculation assumes Dirichlet BC on inflow and outflow boundaries
    double poreVolume = 0.0;
    double fluxInSolid = 0.0, fluxInFluid = 0.0;
    double fluxOutSolid = 0.0, fluxOutFluid = 0.0;
    for (const auto& element : elements(leafGridView, Dune::Partitions::interior))
    {
        poreVolume += element.geometry().volume();

        auto fvGeometry = localView(*gridGeometry);
        fvGeometry.bind(element);

        auto elemVolVars = localView(gridVariables->curGridVolVars());
        elemVolVars.bind(element, fvGeometry, x);

        auto elemFluxVarsCache = localView(gridVariables->gridFluxVarsCache());
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        for (const auto& scvf : scvfs(fvGeometry))
        {
            if (scvf.boundary())
            {
                const auto& globalPos = scvf.ipGlobal();
                if (globalPos[direction] > bBoxMax[direction] - eps)
                {
                    const auto flux = localResidual.computeFlux(*problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    if (problem->marker(scvf.insideScvIdx()))
                        fluxOutFluid += flux;
                    else
                        fluxOutSolid += flux;
                }
                else if (globalPos[direction] < bBoxMin[direction] + eps)
                {
                    const auto flux = localResidual.computeFlux(*problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    if (problem->marker(scvf.insideScvIdx()))
                        fluxInFluid += flux;
                    else
                        fluxInSolid += flux;
                }
            }
        }
    }

    auto fluxIn = fluxInFluid + fluxInSolid;
    auto fluxOut = fluxOutFluid + fluxOutSolid;

    // sum over processes
    const auto& comm = mpiHelper.getCollectiveCommunication();
    if (comm.size() > 1)
    {
        fluxIn = comm.sum(fluxIn);
        fluxOut = comm.sum(fluxOut);
        poreVolume = comm.sum(poreVolume);
    }

    // if (mpiHelper.rank() == 0)
    // {
        // formula to transmissibilities heatflux = -t(T_r - T_l) --> t = -heatflux/(T_r - T_l) = heatflux
        // or effective conductivity heatflux = -lambda_eff*area/length*(T_r - T_l) --> lambda_eff = heatflux/area*length
        // Note that for convenience: (T_l - T_r) = 1

        const auto area = [&](){
            auto dims = bBoxMax - bBoxMin;
            dims[direction] = 1.0;
            return dims[0]*dims[1]*dims[2];
        }();

        const auto volume = [&](){
            const auto dims = bBoxMax - bBoxMin;
            return dims[0]*dims[1]*dims[2];
        }();

        const auto porosity = poreVolume/volume;
        const double appDiffusionCoefficient = std::abs(fluxIn)/area*length;
        const double appTransmissibility = std::abs(fluxIn);
        const auto diffCoeffSolid = getParam<double>("Problem.DiffusionCoefficientSolid");
        const auto diffCoeffVoid = getParam<double>("Problem.DiffusionCoefficientVoid");

        std::cout << "\nHeat conductivity/transmissibility output:\n---------------------\n";
        std::cout << Fmt::format("length {:.3e}, area: {:.3e}, volume: {:.3e}, influx: {:.10e}, outflux: {:.10e}, mass balance error: {:.2f}%\n",
                                  length, area, volume, fluxIn, fluxOut, (fluxIn+fluxOut)*100/std::max(fluxIn, fluxOut));

        std::cout << Fmt::format("ϕ = {:.5e}, κ = {:.10e}, λ_eff = {:.10e}, t_eff = {:.10e}\n",
                                  porosity, diffCoeffVoid/diffCoeffSolid, appDiffusionCoefficient, appTransmissibility);

        std::cout << std::endl;

        // Parameters::print();
    // }

    // evaluation of solid-fluid interaction
    double interfaceTemp = 0.0, interfaceArea = 0.0;
    double couplingFlux = 0.0;
    double poreToGrainDistance = gridGeometry->bBoxMax().two_norm();
    for (const auto& element : elements(leafGridView))
    {
        auto fvGeometry = localView(*gridGeometry);
        auto elemVolVars = localView(gridVariables->curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables->gridFluxVarsCache());
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
        for (const auto& scvf : scvfs(fvGeometry))
        {
            if (!problem->isCoupledScvf(scvf))
                continue;

            if (scvf.insideScvIdx() > scvf.outsideScvIdx())
                continue;

            const auto area = scvf.area();
            const auto interfaceFlux = problem->interfaceFlux(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);
            couplingFlux += area*interfaceFlux;
            interfaceArea += area;
            interfaceTemp += problem->interfaceTemperature(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)*area;
        }
    }
    interfaceTemp /= interfaceArea;

    // compute exact interfacial area
    const auto exactInterfaceArea = [&](){
        const auto R = 1.0;
        const auto dx = length;
        const auto h = R - dx;
        const auto A = 4*M_PI*R*R - 6*(2*M_PI*R*h);
        return A/8.0;
    }();

    const auto fluidTemp = 1.0;
    const auto solidTemp = 0.0;
    const auto fluidThroatTemp = 0.0;
    const auto solidContactTemp = 1.0;

    std::cout << Fmt::format("Ts = {}\n", solidTemp);
    std::cout << Fmt::format("Tf = {}\n", fluidTemp);
    std::cout << Fmt::format("Ti = {}\n", interfaceTemp);
    std::cout << Fmt::format("distance pore-center-grain-center = {}\n", poreToGrainDistance);
    std::cout << Fmt::format("A_exact = {}\n", exactInterfaceArea);
    std::cout << Fmt::format("Q = {}\n", couplingFlux);

    const auto specificCouplingFlux = couplingFlux/exactInterfaceArea;
    const auto lambdaSolid = getParam<double>("Problem.DiffusionCoefficientSolid");
    const auto lambdaFluid = getParam<double>("Problem.DiffusionCoefficientVoid");

    const auto nusseltNumber = specificCouplingFlux*0.5*poreToGrainDistance/(lambdaFluid*(fluidTemp - interfaceTemp));
    const auto biotNumber = specificCouplingFlux*0.5*poreToGrainDistance/(lambdaSolid*(solidTemp - interfaceTemp));

    std::cout << Fmt::format("Nusselt = {}\n", std::abs(nusseltNumber));
    std::cout << Fmt::format("Biot = {}\n", std::abs(biotNumber));
    std::cout << Fmt::format("Kappa = {}\n", lambdaFluid/lambdaSolid);

    const auto lambdaEffIF = lambdaFluid*std::abs(biotNumber)/(lambdaFluid/lambdaSolid + std::abs(biotNumber)/std::abs(nusseltNumber));
    std::cout << Fmt::format("Lambda_eff = Lambda_f Bi / (Kappa + Bi/Nu) = {:.5e}\n", lambdaEffIF);

    const auto totalFaceArea = [&](){
        auto dims = bBoxMax - bBoxMin;
        dims[direction] = 1.0;
        return dims[0]*dims[1]*dims[2];
    }();

    // when kappa != 0 and kappa != oo the effective thermal conductivities actually change
    // since the 3D temperature field looks completely different
    const auto lambdaEffFluid = std::min(std::abs(fluxInFluid), std::abs(fluxOutFluid))/totalFaceArea*length/(lambdaFluid*(fluidThroatTemp - fluidTemp));
    const auto lambdaEffSolid = std::min(std::abs(fluxInSolid), std::abs(fluxOutSolid))/totalFaceArea*length/(lambdaSolid*(solidContactTemp - solidTemp));
    std::cout << Fmt::format("lambdaEffFluid = {}\n", lambdaEffFluid);
    std::cout << Fmt::format("lambdaEffFluid/lambdaFluid = {}\n", lambdaEffFluid/lambdaFluid);
    std::cout << Fmt::format("lambdaEffSolid = {}\n", lambdaEffSolid);
    std::cout << Fmt::format("lambdaEffSolid/lambdaSolid = {}\n", lambdaEffSolid/lambdaSolid);

    const auto logName = getParam<std::string>("Problem.LogFileName", "test.log");
    const auto overlapFactor = getParam<std::array<double, 3>>("Grid.UpperRight")[0];
    std::ofstream logFile(logName);
    logFile << "κ f λ λ_F λ_S, λ_I\n"
            << Fmt::format("{:.10e} {:.10e} {:.10e} {:.10e} {:.10e} {:.10e}\n",
                            diffCoeffVoid/diffCoeffSolid, overlapFactor, appDiffusionCoefficient, lambdaEffFluid, lambdaEffSolid, lambdaEffIF);

    return 0;

}
catch (const Dumux::ParameterException& e)
{
    std::cerr << "ParameterException: " << e;
    return 1;
}
