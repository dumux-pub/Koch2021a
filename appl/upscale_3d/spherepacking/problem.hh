// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TRACER_TEST_PROBLEM_HH
#define DUMUX_TRACER_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

template <class TypeTag>
class TracerTest : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

    //! property that defines whether mole or mass fractions are used
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    TracerTest(std::shared_ptr<const GridGeometry> gridGeometry, const std::vector<char>& marker)
    : ParentType(gridGeometry), marker_(marker)
    {
        direction_ = getParam<int>("Problem.Direction");

        const auto& bBoxMin = this->gridGeometry().bBoxMin();
        const auto& bBoxMax = this->gridGeometry().bBoxMax();
        length_ = bBoxMax[direction_] - bBoxMin[direction_];
        offset_ = bBoxMin[direction_];
        eps_ = 1e-6*length_;

        // compute marker if not coming from grid
        if (marker_.empty())
        {
            marker_.resize(gridGeometry->gridView().size(0));
            for (const auto& element : elements(gridGeometry->gridView()))
            {
                const auto eIdx = gridGeometry->elementMapper().index(element);
                const auto& center = element.geometry().center();
                if (center.two_norm() < 1.0)
                    marker_[eIdx] = 1;
                else
                    marker_[eIdx] = 0;
            }
        }
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        const auto& bBoxMin = this->gridGeometry().bBoxMin();
        const auto& bBoxMax = this->gridGeometry().bBoxMax();

        if (globalPos[direction_] > bBoxMax[direction_] - eps_ || globalPos[direction_] < bBoxMin[direction_] + eps_)
            values.setAllDirichlet();

        return values;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        return initialAtPos(globalPos);
    }

    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        return PrimaryVariables((globalPos[direction_] - offset_)/length_);
    }

    template<class FVGeometry, class ElementVolumeVariables, class ElemFluxVarsCache, class SubControlVolumeFace>
    Scalar interfaceFlux(const Element& element,
                         const FVGeometry& fvgeometry,
                         const ElementVolumeVariables& elemVolVars,
                         const ElemFluxVarsCache& fluxCache,
                         const SubControlVolumeFace& scvf) const
    {
        const auto sign = marker(scvf.insideScvIdx()) ? 1.0 : -1.0;
        const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];
        const auto insideTemp = insideVolVars.moleFraction(0, 0)*insideVolVars.molarDensity(0);
        const auto outsideTemp = outsideVolVars.moleFraction(0, 0)*outsideVolVars.molarDensity(0);
        const auto dx = (fvgeometry.scv(scvf.insideScvIdx()).center() - scvf.center()).two_norm();
        const auto insideLambda = insideVolVars.diffusionCoefficient(0,0,0);
        const auto outsideLambda = outsideVolVars.diffusionCoefficient(0,0,0);

        return sign*(outsideTemp - insideTemp)/dx*insideLambda*outsideLambda/(insideLambda + outsideLambda);
    }

    template<class FVGeometry, class ElementVolumeVariables, class ElemFluxVarsCache, class SubControlVolumeFace>
    Scalar interfaceTemperature(const Element& element,
                                const FVGeometry& fvgeometry,
                                const ElementVolumeVariables& elemVolVars,
                                const ElemFluxVarsCache& fluxCache,
                                const SubControlVolumeFace& scvf) const
    {
        const auto& insideVolVars = elemVolVars[scvf.insideScvIdx()];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];
        const auto insideTemp = insideVolVars.moleFraction(0, 0)*insideVolVars.molarDensity(0);
        const auto outsideTemp = outsideVolVars.moleFraction(0, 0)*outsideVolVars.molarDensity(0);
        const auto insideLambda = insideVolVars.diffusionCoefficient(0,0,0);
        const auto outsideLambda = outsideVolVars.diffusionCoefficient(0,0,0);

        return (outsideTemp*outsideLambda + insideTemp*insideLambda)/(insideLambda + outsideLambda);
    }

    template<class SubControlVolume>
    bool isFluidScv(const SubControlVolume& scv) const
    { return marker(scv.dofIndex()); }

    template<class SubControlVolumeFace>
    bool isCoupledScvf(const SubControlVolumeFace& scvf) const
    {
        if (scvf.boundary())
            return false;
        return marker(scvf.insideScvIdx()) != marker(scvf.outsideScvIdx());
    }

    bool marker(std::size_t eIdx) const
    { return marker_[eIdx] == 0; }

private:
    std::vector<char> marker_;
    int direction_;
    Scalar length_;
    Scalar offset_;
    Scalar eps_;
};

} // end namespace Dumux

#endif
