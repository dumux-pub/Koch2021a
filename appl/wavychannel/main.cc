// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A test problem for the coupled Stokes/Darcy problem (1p).
 */

#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/dynvector.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/integrate.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/nonlinear/findscalarroot.hh>

#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include "properties.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using FreeFlowTypeTag = Properties::TTag::FreeFlowModel;
    using SolidEnergyTypeTag = Properties::TTag::HeatModel;

    // Create the host grid
    using HostGrid = typename GetProp<FreeFlowTypeTag, Properties::Grid>::HostGrid;
    GridManager<HostGrid> gridManager;
    gridManager.init();

    // Create sub grids for each domain
    const auto poreRadius = getParam<double>("Grid.PoreRadius");
    const auto channelRadius = getParam<double>("Grid.ChannelRadius");
    const auto numberOfWaves = getParam<double>("Grid.NumberOfWaves");
    const auto lengthDimensions = getParam<std::array<double, 2>>("Grid.Positions1");
    const auto radialDimensions = getParam<std::array<double, 2>>("Grid.Positions0");
    const auto cellsX = getParam<double>("Grid.Cells0");
    const auto length = (lengthDimensions[1]-lengthDimensions[0]);
    const auto cellXSize = radialDimensions[1]/cellsX;
    const auto yOffset = lengthDimensions[0];
    const auto waveLength = length/numberOfWaves;

    const auto waveFunctionPosX = [&](const auto posY){
        const auto y = posY - yOffset;
        const auto A = poreRadius - channelRadius;
        return channelRadius + A - A*std::cos(2*M_PI*y/waveLength);
    };

    const auto elementSelectorSolid = [&](const auto& element){
        const auto pos = element.geometry().center();
        return pos[0] > waveFunctionPosX(pos[1]);
    };

    const auto elementSelectorFluid = [&](const auto& element){
        const auto pos = element.geometry().center();
        return pos[0] <= waveFunctionPosX(pos[1]);
    };

    GridManager<GetPropType<FreeFlowTypeTag, Properties::Grid>> solidEnergyGridManager, freeFlowGridManager;
    solidEnergyGridManager.init(gridManager.grid(), elementSelectorSolid);
    freeFlowGridManager.init(gridManager.grid(), elementSelectorFluid);

    // we compute on the leaf grid view
    const auto& solidEnergyGridView = solidEnergyGridManager.grid().leafGridView();
    const auto& freeFlowGridView = freeFlowGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FreeFlowGridGeometry = GetPropType<FreeFlowTypeTag, Properties::GridGeometry>;
    auto freeFlowGridGeometry = std::make_shared<FreeFlowGridGeometry>(freeFlowGridView);
    freeFlowGridGeometry->update();
    using SolidEnergyGridGeometry = GetPropType<SolidEnergyTypeTag, Properties::GridGeometry>;
    auto solidEnergyGridGeometry = std::make_shared<SolidEnergyGridGeometry>(solidEnergyGridView);
    solidEnergyGridGeometry->update();

    using Traits = StaggeredMultiDomainTraits<FreeFlowTypeTag, FreeFlowTypeTag, SolidEnergyTypeTag>;

    // the coupling manager
    using CouplingManager = FreeFlowSolidEnergyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(freeFlowGridGeometry, solidEnergyGridGeometry);

    // the indices
    constexpr auto freeFlowCellCenterIdx = CouplingManager::freeFlowCellCenterIdx;
    constexpr auto freeFlowFaceIdx = CouplingManager::freeFlowFaceIdx;
    constexpr auto solidEnergyIdx = CouplingManager::solidEnergyIdx;

    // the problem (initial and boundary conditions)
    using FreeFlowProblem = GetPropType<FreeFlowTypeTag, Properties::Problem>;
    auto freeFlowProblem = std::make_shared<FreeFlowProblem>(freeFlowGridGeometry, couplingManager);
    using SolidEnergyProblem = GetPropType<SolidEnergyTypeTag, Properties::Problem>;
    auto solidEnergyProblem = std::make_shared<SolidEnergyProblem>(solidEnergyGridGeometry, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[freeFlowCellCenterIdx].resize(freeFlowGridGeometry->numCellCenterDofs());
    sol[freeFlowFaceIdx].resize(freeFlowGridGeometry->numFaceDofs());
    sol[solidEnergyIdx].resize(solidEnergyGridGeometry->numDofs());

    // get a solution vector storing references to the two Stokes solution vectors
    auto stokesSol = partial(sol, freeFlowFaceIdx, freeFlowCellCenterIdx);

    // apply initial solution for instationary problems
    freeFlowProblem->applyInitialSolution(stokesSol);
    solidEnergyProblem->applyInitialSolution(sol[solidEnergyIdx]);

    couplingManager->init(freeFlowProblem, solidEnergyProblem, sol);

    // the grid variables
    using FreeFlowGridVariables = GetPropType<FreeFlowTypeTag, Properties::GridVariables>;
    auto freeFlowGridVariables = std::make_shared<FreeFlowGridVariables>(freeFlowProblem, freeFlowGridGeometry);
    freeFlowGridVariables->init(stokesSol);
    using SolidEnergyGridVariables = GetPropType<SolidEnergyTypeTag, Properties::GridVariables>;
    auto solidEnergyGridVariables = std::make_shared<SolidEnergyGridVariables>(solidEnergyProblem, solidEnergyGridGeometry);
    solidEnergyGridVariables->init(sol[solidEnergyIdx]);

    // intialize the vtk output module
    StaggeredVtkOutputModule<FreeFlowGridVariables, decltype(stokesSol)> freeFlowVtkWriter(*freeFlowGridVariables, stokesSol, freeFlowProblem->name());
    GetPropType<FreeFlowTypeTag, Properties::IOFields>::initOutputModule(freeFlowVtkWriter);
    freeFlowVtkWriter.write(0.0);

    VtkOutputModule<SolidEnergyGridVariables, GetPropType<SolidEnergyTypeTag, Properties::SolutionVector>> solidEnergyVtkWriter(*solidEnergyGridVariables, sol[solidEnergyIdx],  solidEnergyProblem->name());
    GetPropType<SolidEnergyTypeTag, Properties::IOFields>::initOutputModule(solidEnergyVtkWriter);
    solidEnergyVtkWriter.write(0.0);

    // the assembler for a stationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(freeFlowProblem, freeFlowProblem, solidEnergyProblem),
                                                 std::make_tuple(freeFlowGridGeometry->faceFVGridGeometryPtr(),
                                                                 freeFlowGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 solidEnergyGridGeometry),
                                                 std::make_tuple(freeFlowGridVariables->faceGridVariablesPtr(),
                                                                 freeFlowGridVariables->cellCenterGridVariablesPtr(),
                                                                 solidEnergyGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    nonLinearSolver.solve(sol);

    freeFlowVtkWriter.write(1.0);
    solidEnergyVtkWriter.write(1.0);

    ////////////////////////////////////////////////////////////
    // Evaluation
    ////////////////////////////////////////////////////////////

    // interfacial area by rotational surface formula
    const auto waveFunctionPosXDeriv = [&](const auto t){
        const auto y = t - yOffset;
        const auto A = poreRadius - channelRadius;
        return A*2*M_PI/waveLength*std::sin(2*M_PI*y/waveLength);
    };

    const auto integrand = [&](const auto t){
        const auto xt = waveFunctionPosX(t);
        const auto dxdt = waveFunctionPosXDeriv(t);
        return 2*M_PI*xt*std::sqrt(dxdt*dxdt + 1);
    };

    const auto interfacialArea = integrateScalarFunction(integrand, 0.0, waveLength);
    std::cout << "Interfacial area: " << interfacialArea << " m^2\n";

    // compute average temperatures
    Dune::DynamicVector<double> solidTemp(numberOfWaves+1); solidTemp = 0.0;
    Dune::DynamicVector<double> solidVolume(numberOfWaves+1); solidVolume = 0.0;
    Dune::DynamicVector<double> interfacicalTemp(numberOfWaves*2); interfacicalTemp = 0.0;
    Dune::DynamicVector<double> couplingFlux(numberOfWaves*2); couplingFlux = 0.0;
    Dune::DynamicVector<double> discreteInterfacialArea(numberOfWaves*2); discreteInterfacialArea = 0.0;
    double boundaryFlux = 0.0, totalCouplingFlux = 0.0;

    const auto computeSolidIndex = [&](const auto& scv){
        const auto y = scv.center()[1];
        const auto ratio = (y + 0.5*waveLength)/waveLength;
        return int(std::floor(ratio));
    };

    const auto computeInterfaceIndex = [&](const auto& scvf){
        const auto y = scvf.center()[1];
        const auto ratio = y/(0.5*waveLength);
        return int(std::floor(ratio));
    };

    // https://en.wikipedia.org/wiki/List_of_centroids: Centroid of annular sectors
    double volumeSolidGrain = 0.0;
    Dune::FieldVector<double, 2> centroidSolidGrain(0.0);
    for (const auto& element : elements(solidEnergyGridView))
    {
        auto fvGeometry = localView(*solidEnergyGridGeometry);
        fvGeometry.bind(element);

        using Extrusion = typename SolidEnergyGridGeometry::Extrusion;
        for (const auto& scv : scvs(fvGeometry))
        {
            const auto vol = Extrusion::volume(scv);
            const auto solidIndex = computeSolidIndex(scv);
            solidTemp[solidIndex] += sol[solidEnergyIdx][scv.dofIndex()][0]*vol;
            solidVolume[solidIndex] += vol;

            const auto& center = scv.center();
            if (scv.center()[1] > 0.5*waveLength && scv.center()[1] < 1.5*waveLength)
            {
                auto centroid = center;
                const auto r2 = scv.center()[0] + 0.5*cellXSize;
                const auto r1 = scv.center()[0] - 0.5*cellXSize;
                centroid[0] = 2.0*0.5*std::sqrt(2.0)/(3.0*M_PI*0.25)*(r2*r2*r2 - r1*r1*r1)/(r2*r2 - r1*r1);
                centroidSolidGrain.axpy(0.25*vol, centroid);
                volumeSolidGrain += 0.25*vol;
            }
        }

        auto elemVolVars = localView(solidEnergyGridVariables->curGridVolVars());
        auto elemFluxVarsCache = localView(solidEnergyGridVariables->gridFluxVarsCache());
        elemVolVars.bind(element, fvGeometry, sol[solidEnergyIdx]);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
        for (const auto& scvf : scvfs(fvGeometry))
        {
            if (!scvf.boundary())
                continue;

            static const auto avgType = FreeFlowHeatCouplingOptions::stringToEnum(getParamFromGroup<std::string>("", "Problem.DiffCoeffAvgType", "FreeFlowOnly"));

            const auto area = Extrusion::area(scvf);
            const auto neumannFlux = solidEnergyProblem->neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)[0];
            const auto interfaceIndex = computeInterfaceIndex(scvf);
            if (couplingManager->isCoupledEntity(CouplingManager::solidEnergyIdx, scvf))
            {
                couplingFlux[interfaceIndex] += area*neumannFlux;
                discreteInterfacialArea[interfaceIndex] += area;
                interfacicalTemp[interfaceIndex] += couplingManager->couplingData().interfacialTemperature(element, fvGeometry, elemVolVars, scvf, avgType)*area;
            }
            else
                boundaryFlux += area*neumannFlux;
        }
    }

    // pore and grain centers
    const auto [xmin, xmax] = getParam<std::array<double, 2>>("Grid.Positions0");
    centroidSolidGrain /= volumeSolidGrain;
    Dune::FieldVector<double, 2> centroidFluidPore({xmin, 0.5*waveLength});
    Dune::FieldVector<double, 2> centroidFluidThroat({xmin, 0.0});
    std::cout << "centroid solid grain quarter rotation: " << centroidSolidGrain << "\n";
    std::cout << "centroid fluid pore: " << centroidFluidPore << "\n";
    const auto poreGrainCenterDistance = (centroidSolidGrain - centroidFluidPore).two_norm();
    std::cout << "Pore-grain-center-distance: " << poreGrainCenterDistance << " m\n";
    const auto throatGrainCenterDistance = (centroidSolidGrain - centroidFluidThroat).two_norm();
    std::cout << "Throat-grain-center-distance: " << throatGrainCenterDistance << " m\n";

    const auto connectionPoreGrainPosX = [&](const auto posY){
        const auto y = posY - yOffset;
        return centroidSolidGrain[0] - y/(0.5*waveLength)*(centroidSolidGrain[0] - 0.0);
    };
    const auto connectionPoreGrainPosY = [&](const auto posX){
        return yOffset + (posX-centroidSolidGrain[0])/(0.0-centroidSolidGrain[0])*0.5*waveLength;
    };
    const auto residual = [&](const auto posY){
        return connectionPoreGrainPosX(posY) - waveFunctionPosX(posY);
    };
    const auto intersectionPointY = findScalarRootBrent(yOffset, yOffset+0.5*waveLength, residual);
    const auto intersectionPointX = connectionPoreGrainPosX(intersectionPointY);

    std::cout << "intersectionPointY: " << intersectionPointY << " m\n";
    std::cout << "intersectionPointX: " << intersectionPointX << " m\n";
    const auto poreToInterface = std::hypot(intersectionPointX-xmin, yOffset+0.5*waveLength-intersectionPointY);
    const auto interfaceToGrain = std::hypot(xmax-intersectionPointX, yOffset-intersectionPointY);
    std::cout << "Pore-interface-distance: " << poreToInterface << " m\n";
    std::cout << "Interface-grain-distance: " << interfaceToGrain << " m\n";
    std::cout << "Pore-grain-center-distance: " << poreToInterface + interfaceToGrain << " m\n";
    std::cout << "Ratio: poreToInterface/totaldist" << poreToInterface/(poreToInterface + interfaceToGrain) << " m\n";

    for (int i = 0; i < solidTemp.size(); ++i)
    {
        solidTemp[i] /= solidVolume[i];
        std::cout << Fmt::format("({}) T_s = {} K,  V_s = {} m^3\n", i, solidTemp[i], solidVolume[i]);
    }

    for (int i = 0; i < interfacicalTemp.size(); ++i)
    {
        interfacicalTemp[i] /= discreteInterfacialArea[i];
        std::cout << Fmt::format("({}) T_I = {} K\n", i, interfacicalTemp[i]);
        std::cout << Fmt::format("({}) Coupling heat flux = {} W\n", i, couplingFlux[i]);
        std::cout << Fmt::format("({}) Specific coupling heat flux = {} W/m^2\n", i, couplingFlux[i]/interfacialArea*2);
        totalCouplingFlux += couplingFlux[i];
    }

    std::cout << Fmt::format("Boundary heat flux (solid) = {} W\n", boundaryFlux);
    std::cout << Fmt::format("Total coupling heat flux (solid) = {} W\n", totalCouplingFlux);


    Dune::DynamicVector<double> fluidTemp(numberOfWaves); fluidTemp = 0.0;
    Dune::DynamicVector<double> fluidThroatTemp(numberOfWaves-1); fluidThroatTemp = 0.0;
    Dune::DynamicVector<double> fluidThroatInterfaceTemp(numberOfWaves-1); fluidThroatInterfaceTemp = 0.0;
    Dune::DynamicVector<double> fluidVolume(numberOfWaves); fluidVolume = 0.0;

    couplingFlux = 0.0;
    discreteInterfacialArea = 0.0;
    totalCouplingFlux = 0.0;

    auto elemVolVars = localView(freeFlowGridVariables->curGridVolVars());
    auto elemFluxVarsCache = localView(freeFlowGridVariables->gridFluxVarsCache());
    auto elemFaceVars = localView(freeFlowGridVariables->curGridFaceVars());

    const auto computeFluidIndex = [&](const auto& scv){
        const auto y = scv.center()[1];
        const auto ratio = y/waveLength;
        return int(std::floor(ratio));
    };

    for (const auto& element : elements(freeFlowGridView))
    {
        auto fvGeometry = localView(*freeFlowGridGeometry);
        fvGeometry.bind(element);

        using Extrusion = typename FreeFlowGridGeometry::Extrusion;
        for (const auto& scv : scvs(fvGeometry))
        {
            const auto vol = Extrusion::volume(scv);
            const auto fluidIndex = computeFluidIndex(scv);
            fluidTemp[fluidIndex] += sol[freeFlowCellCenterIdx][scv.elementIndex()][1]*vol;
            fluidVolume[fluidIndex] += vol;
        }

        elemVolVars.bind(element, fvGeometry, stokesSol);
        elemFaceVars.bind(element, fvGeometry, stokesSol);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
        for (const auto& scvf : scvfs(fvGeometry))
        {
            if (!scvf.boundary())
                continue;

            using Indices = typename GetPropType<FreeFlowTypeTag, Properties::ModelTraits>::Indices;
            const auto area = Extrusion::area(scvf);
            if (couplingManager->isCoupledEntity(CouplingManager::freeFlowIdx, scvf))
            {
                const auto neumannFlux = freeFlowProblem->neumann(element, fvGeometry, elemVolVars, elemFaceVars, scvf)[Indices::energyEqIdx];
                const auto interfaceIndex = computeInterfaceIndex(scvf);
                couplingFlux[interfaceIndex] += area*neumannFlux;
                discreteInterfacialArea[interfaceIndex] += area;
            }
        }

    }

    for (int i = 0; i < fluidTemp.size(); ++i)
    {
        fluidTemp[i] /= fluidVolume[i];
        std::cout << Fmt::format("({}) T_f = {} K,  V_f = {} m^3\n", i, fluidTemp[i], fluidVolume[i]);
    }

    for (int i = 0, k = 1; i < fluidThroatTemp.size(); ++i, k+=2)
    {
        fluidThroatTemp[i] = 0.5*(fluidTemp[i] + fluidTemp[i+1]);
        std::cout << Fmt::format("({}) T_throat_f = {} K\n", i, fluidThroatTemp[i]);
        fluidThroatInterfaceTemp[i] = 0.5*(interfacicalTemp[k] + interfacicalTemp[k+1]);
    }

    for (int i = 0; i < interfacicalTemp.size(); ++i)
    {
        std::cout << Fmt::format("({}) Coupling heat flux = {} W\n", i, couplingFlux[i]);
        std::cout << Fmt::format("({}) Specific coupling heat flux = {} W/m^2\n", i, couplingFlux[i]/interfacialArea*2);
        totalCouplingFlux += couplingFlux[i];
    }

    const auto porosity = fluidVolume[0]/(2*solidVolume[0] + fluidVolume[0]);
    std::cout << Fmt::format("porosity = {}\n", porosity);

    const auto lambdaSolid = getParam<double>("2.Component.SolidThermalConductivity");
    const auto lambdaFluid = GetPropType<FreeFlowTypeTag, Properties::FluidSystem>::thermalConductivity(0.0, 0.0);

    auto nusselt = interfacicalTemp; nusselt = 0.0;
    auto biot = interfacicalTemp; biot = 0.0;
    auto throatLambda = fluidThroatTemp; throatLambda = 0.0;
    auto upstreamLambda = fluidThroatTemp; upstreamLambda = 0.0;
    auto throatNusselt = fluidThroatTemp; throatNusselt = 0.0;
    auto throatBiot = fluidThroatTemp; throatBiot = 0.0;
    for (int i = 0; i < interfacicalTemp.size(); ++i)
    {
        const auto fi = int(std::round(i/5.0*2.0));
        nusselt[i] = std::abs(couplingFlux[i]/interfacialArea*2*poreToInterface/(lambdaFluid*(fluidTemp[fi] - interfacicalTemp[i])));
        std::cout << Fmt::format("(i{}->f{}) Nusselt = {}, dT = {}\n", i, fi, nusselt[i], fluidTemp[fi] - interfacicalTemp[i]);

        const auto si = int(std::round(i/5.0*3.0));
        biot[i] = std::abs(couplingFlux[i]/interfacialArea*2*interfaceToGrain/(lambdaSolid*(solidTemp[si] - interfacicalTemp[i])));
        std::cout << Fmt::format("(i{}->s{}) Biot = {}, dT = {}\n", i, si, biot[i], solidTemp[si] - interfacicalTemp[i]);
    }

    for (int i = 0, k = 1; i < fluidThroatTemp.size(); ++i, k+=2)
    {
        const auto specificFlux = std::abs((couplingFlux[k]+couplingFlux[k+1])/interfacialArea);
        throatLambda[i] = std::abs(specificFlux*throatGrainCenterDistance/(fluidThroatTemp[i] - solidTemp[i+1]));
        upstreamLambda[i] = std::abs(specificFlux*throatGrainCenterDistance/(fluidTemp[i] - solidTemp[i+1]));
        throatNusselt[i] = std::abs(specificFlux*0.5*interfaceToGrain/(lambdaFluid*(fluidThroatTemp[i] - fluidThroatInterfaceTemp[i])));
        throatBiot[i] = std::abs(specificFlux*0.5*interfaceToGrain/(lambdaSolid*(solidTemp[i+1] - fluidThroatInterfaceTemp[i])));
        std::cout << Fmt::format("(throat {}->i {} + i {}->s {}) throatLambda = {}, throatNusselt = {}, throatBiot = {}\n",
                                  i, k, k+1, i+1, throatLambda[i], throatNusselt[i], throatBiot[i]);
    }

    // what would be the flux if we use upstream and downstream temperatures
    const auto Qup = throatLambda[2]*0.5*interfacialArea/throatGrainCenterDistance*(fluidTemp[2]-solidTemp[3]);
    const auto Qdown = throatLambda[2]*0.5*interfacialArea/throatGrainCenterDistance*(fluidTemp[3]-solidTemp[3]);
    std::cout << Fmt::format("(EXACT coupling flux: up {} + down {} = {}\n", couplingFlux[5], couplingFlux[6], couplingFlux[5]+couplingFlux[6]);
    std::cout << Fmt::format("(THROAT coupling flux with lambda: up {} + down {} = {}\n", Qup, Qdown, Qup+Qdown);
    const auto Qup2 = upstreamLambda[2]*0.5*interfacialArea/throatGrainCenterDistance*(fluidTemp[2]-solidTemp[3]);
    const auto Qdown2 = upstreamLambda[2]*0.5*interfacialArea/throatGrainCenterDistance*(fluidTemp[3]-solidTemp[3]);
    std::cout << Fmt::format("(UPSTREAM coupling flux with lambda: up {} + down {} = {}\n", Qup2, Qdown2, Qup2+Qdown2);

    std::cout << Fmt::format("Kappa = {}\n", lambdaFluid/lambdaSolid);

    ////////////////////////////////////////////////////////////
    // write output file
    ////////////////////////////////////////////////////////////
    const auto logFileName = getParam<std::string>("Problem.LogFileName");
    std::ofstream logFile(logFileName);
    logFile << "velocity,channel_length,pore_radius,channel_radius,porosity,lambda_s,cp_s,rho_s,lambda_f,kappa,nu"
            << ",Re,Pe,Pr,RePore,PePore,Nu11,Nu12,Bi11,Bi12"
            << ",lambdaEff1,lambdaEff2,lambdaEff3,lambdaEff4"
            << ",throatNu1,throatNu2,throatNu3,throatNu4"
            << ",throatBiot1,throatBiot2,throatBiot3,throatBiot4"
            << ",xf,xs,throatgraindist,interface_area\n";

    const auto velocity = getParam<double>("FreeFlow.Problem.InletVelocity");
    const auto cp_s = getParam<double>("2.Component.SolidHeatCapacity");
    const auto rho_s = getParam<double>("2.Component.SolidDensity");

    logFile << Fmt::format("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n",
        velocity, length, poreRadius, channelRadius,
        porosity, lambdaSolid, cp_s, rho_s, lambdaFluid, lambdaFluid/lambdaSolid, freeFlowProblem->nu(),
        freeFlowProblem->Re(),freeFlowProblem->Pe(),freeFlowProblem->Pr(),freeFlowProblem->RePore(),freeFlowProblem->PePore(),
        nusselt[2],nusselt[3],biot[2],biot[3],
        throatLambda[1],throatLambda[2],throatLambda[3],throatLambda[4],
        throatNusselt[1],throatNusselt[2],throatNusselt[3],throatNusselt[4],
        throatBiot[1],throatBiot[2],throatBiot[3],throatBiot[4],
        poreToInterface,interfaceToGrain,throatGrainCenterDistance,interfacialArea
    );

    // print dumux end message
    if (mpiHelper.rank() == 0)
        Parameters::print();

    return 0;

}
