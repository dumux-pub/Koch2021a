#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import matplotlib

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

matplotlib.rc('font', **font)

logMasterFileNames = [
    "logfiles/76434156-1e99-4945-a1b2-26ed645a4ec1.master.log",
    "logfiles/6b0e7935-1734-497b-84e4-7f8a47daeeb2.master.log",
    "logfiles/a00d90cc-7785-4fa6-a6a4-0c84697f5bfc.master.log",
]

data = []
for i, masterFileName in enumerate(logMasterFileNames):
    logFiles = []
    with open(masterFileName, "r") as master:
        for line in master.readlines():
            if line.startswith("logfiles"):
                logFiles.append(line.strip())
            else:
                print(line)

    print(logFiles)

    velocities = np.logspace(np.log10(0.0005), np.log10(5.0), 17)
    data.append([])
    for j, vel in enumerate(velocities):
        dataPoint = np.genfromtxt(logFiles[j], names=True, delimiter=",")
        data[i].append([dataPoint["Re"],
                    dataPoint["lambdaEff1"], dataPoint["lambdaEff2"], dataPoint["lambdaEff3"], dataPoint["lambdaEff4"],
                    dataPoint["throatNu1"], dataPoint["throatNu2"], dataPoint["throatNu3"], dataPoint["throatNu4"],
                    dataPoint["throatBiot1"], dataPoint["throatBiot2"], dataPoint["throatBiot3"], dataPoint["throatBiot4"],
                    dataPoint["kappa"], dataPoint["lambda_f"], dataPoint["channel_radius"], dataPoint["throatgraindist"], dataPoint["lambda_s"]])
        print(dataPoint["channel_length"])

    data[i] = np.array(data[i]).T
data = np.array(data)

lambda_f = data[:,14,:]
lambda_s = data[:,17,:]

lambda_avg = lambda_f*lambda_s/(lambda_f + lambda_s)
channel_radius = data[:,15,:]
throatgrain = data[:,16,:]

# Compute effective lambda from Nu and Bi
kappa = data[:,13,:]
# multiply data by 2 because we changed definition to be always relative to
# the solid-throat distance
Nu = data[:,7,:]*2
Bi = data[:,11,:]*2
effectiveLambda = Bi*lambda_f/(Bi/Nu + kappa)

Nu = data[:,6,:]*2
Bi = data[:,10,:]*2
effectiveLambda2 = Bi*lambda_f/(Bi/Nu + kappa)

def myfunc(x):
    return 1.0 + 0.9*x**0.4

def myfunc2(x):
    return 1.0 + 0.75*x**0.4

def myfunc3(x):
    return 1.0 + 0.3*x**0.4

color = plt.rcParams['axes.prop_cycle'].by_key()['color']

# throat 2 is the third throat (not counting the inlet)
fig, ax = plt.subplots(1, 3, figsize=(12,4))
for i, logFile in enumerate(logMasterFileNames):
    print(np.maximum(lambda_f[i], lambda_s[i]))
    ax[0].plot(data[i,0,:], effectiveLambda2[i], color=color[i], label="$\kappa$={:.2g}".format(kappa[i,0]))
    ax[1].plot(data[i,0,:], data[i,6,:]*2, color=color[i], label="$\kappa$={:.2g}".format(kappa[i,0]))
    ax[2].plot(data[i,0,:], data[i,10,:]*2, color=color[i], label="$\kappa$={:.2g}".format(kappa[i,0]))

ax[0].plot(data[0,0,:], myfunc3(data[0,0,:]), "--", color=color[0], label="$1.0 + 0.3\mathrm{Re}^{0.4}$")
ax[0].plot(data[0,0,:], myfunc(data[0,0,:]), "--", color=color[2], label="$1.0 + 0.9\mathrm{Re}^{0.4}$")
ax[0].plot(data[0,0,:], myfunc2(data[0,0,:]), "--", color=color[1], label="$1.0 + 0.75\mathrm{Re}^{0.4}$")

ax[0].set_title(r"$\lambda_\mathrm{conv}$")
ax[1].set_title(r"Nu$_\mathbb{T}$")
ax[2].set_title(r"$b_{s,\mathbb{T}}$")

ax[0].set_xlabel(r"Re$_\mathbb{T}$")
ax[1].set_xlabel(r"Re$_\mathbb{T}$")
ax[2].set_xlabel(r"Re$_\mathbb{T}$")

ax[0].set_xscale("log")
ax[1].set_xscale("log")
ax[2].set_xscale("log")

for axx in ax:
    axx.xaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:g}'.format(y)))

ax[0].legend(fontsize=12)
ax[1].legend(fontsize=12)
ax[2].legend(fontsize=12)
plt.show()

fig.tight_layout()
fig.savefig("heattransfer_reynolds_kappa.pdf")
