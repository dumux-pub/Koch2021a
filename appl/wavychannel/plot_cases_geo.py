#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import matplotlib

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

matplotlib.rc('font', **font)

logMasterFileNames = [
    "logfiles3/4c6ed74c-ce99-48fb-94cb-e0978f4d0edc.master.log",
    "logfiles3/46bb3b8b-20a3-4f0c-afd7-720e4bcbaa81.master.log",
    "logfiles3/ff848b93-bab6-46f2-ab82-3b207e5eb23a.master.log",
    "logfiles3/243ef080-3285-43d1-aeb4-c7332b6c9a04.master.log"
]

data = []
for i, masterFileName in enumerate(logMasterFileNames):
    logFiles = []
    with open(masterFileName, "r") as master:
        for line in master.readlines():
            if line.startswith("logfiles"):
                logFiles.append(line.strip())
            else:
                print(line)

    print(logFiles)

    data.append([])
    for logFile in logFiles:
        dataPoint = np.genfromtxt(logFile, names=True, delimiter=",")
        data[i].append([dataPoint["Re"],
                    dataPoint["lambdaEff1"], dataPoint["lambdaEff2"], dataPoint["lambdaEff3"], dataPoint["lambdaEff4"],
                    dataPoint["throatNu1"], dataPoint["throatNu2"], dataPoint["throatNu3"], dataPoint["throatNu4"],
                    dataPoint["throatBiot1"], dataPoint["throatBiot2"], dataPoint["throatBiot3"], dataPoint["throatBiot4"],
                    dataPoint["kappa"], dataPoint["lambda_f"], dataPoint["channel_radius"], dataPoint["throatgraindist"], dataPoint["lambda_s"], dataPoint["pore_radius"]])

    data[i] = np.array(data[i]).T
data = np.array(data)

lambda_f = data[:,14,:]
lambda_s = data[:,17,:]
channel_radius = data[:,15,:]
throatgrain = data[:,16,:]

# Compute effective lambda from Nu and Bi
kappa = data[:,13,:]
Nu = data[:,7,:]*2
Bi = data[:,11,:]*2
effectiveLambda = Bi*lambda_f/(Bi/Nu + kappa)

Nu = data[:,6,:]*2
Bi = data[:,10,:]*2
effectiveLambda2 = Bi*lambda_f/(Bi/Nu + kappa)

pore_radius = data[:,18,:]

def myfunc(x):
    return 1.0 + 0.9*x**0.4

color = plt.rcParams['axes.prop_cycle'].by_key()['color']
fig, ax = plt.subplots(1, 3, figsize=(12,4))
for i, logFile in enumerate(logMasterFileNames):
    print(np.maximum(lambda_f[i], lambda_s[i]))
    ax[0].plot(data[i,0,:], effectiveLambda2[i], color=color[i], label="$R_p$={:.2g}".format(pore_radius[i,0]))
    ax[1].plot(data[i,0,:], data[i,6,:]*2, color=color[i], label="$R_p$={:.2g}".format(pore_radius[i,0]))
    ax[2].plot(data[i,0,:], data[i,10,:]*2, color=color[i], label="$R_p$={:.2g}".format(pore_radius[i,0]))

ax[0].plot(data[0,0,:], myfunc(data[0,0,:]), "--", color="k", label="$1.0 + 0.9\mathrm{Re}^{0.4}$")

ax[0].set_title(r"$\lambda_\mathrm{conv}$")
ax[1].set_title(r"Nu$_\mathbb{T}$")
ax[2].set_title(r"$b_{s,\mathbb{T}}$")

ax[0].set_xlabel(r"Re$_\mathbb{T}$")
ax[1].set_xlabel(r"Re$_\mathbb{T}$")
ax[2].set_xlabel(r"Re$_\mathbb{T}$")

ax[0].set_xscale("log")
ax[1].set_xscale("log")
ax[2].set_xscale("log")

for axx in ax:
    axx.xaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:g}'.format(y)))

ax[0].legend(fontsize=12)
ax[1].legend(fontsize=12)
ax[2].legend(fontsize=12)
plt.show()

fig.tight_layout()
fig.savefig("heattransfer_reynold_geo.pdf")
