// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_FREEFLOW_SUBPROBLEM_HH
#define DUMUX_FREEFLOW_SUBPROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>

// for FreeFlowHeatCouplingOptions
#include <dumux/multidomain/boundary/freeflowsolidenergy/couplingmanager.hh>

namespace Dumux {

/*!
 * \brief Freeflow subproblem for conjugate heat transfer
 */
template <class TypeTag>
class FreeFlowSubProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = NavierStokesBoundaryTypes<PrimaryVariables::size()>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    FreeFlowSubProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "FreeFlow"), couplingManager_(couplingManager)

    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        inletVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletVelocity");
        inletTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletTemperature");
        channelRadius_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.ChannelRadius");

        const auto poreRadius = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.PoreRadius");
        const auto poreVelocity = (channelRadius_*channelRadius_)/(poreRadius*poreRadius)*inletVelocity_;

        FluidState fluidState; fluidState.setPressure(0, 1.0e5); fluidState.setTemperature(inletTemperature_);
        const auto rho = FluidSystem::density(fluidState, 0);
        const auto nu = FluidSystem::viscosity(fluidState, 0)/rho;
        const auto lambda = FluidSystem::thermalConductivity(fluidState, 0);
        const auto cp = FluidSystem::heatCapacity(fluidState, 0);

        Re_ = 2*channelRadius_*inletVelocity_/nu;
        RePore_ = 2*poreRadius*poreVelocity/nu;
        Pe_ = 2*channelRadius_*inletVelocity_*rho*cp/lambda;
        PePore_ = 2*poreRadius*poreVelocity*rho*cp/lambda;
        Pr_ = nu*rho*cp/lambda;
        nu_ = nu;
        rho_ = rho;

        std::cout << "-- Kinematic viscosity: " << nu_ << "\n";
        std::cout << "-- Density: " << rho_ << "\n";
        std::cout << "-- Reynolds number: " << Re_ << "\n";
        std::cout << "-- Prandlt number: " <<  Pr_ << "\n";
        std::cout << "-- Péclet number (Re*Pr): " << Pe_ << "\n";
        std::cout << "-- Pore velocity: " << poreVelocity << "\n";
        std::cout << "-- Reynolds number pore: " << RePore_ << "\n";
        std::cout << "-- Péclet number pore: " << PePore_ << "\n";
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        const auto& globalPos = scvf.dofPosition();

        // inlet
        if (onLowerBoundary_(globalPos))
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setDirichlet(Indices::temperatureIdx);
        }
        //  outlet
        else if (onUpperBoundary_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
            values.setOutflow(Indices::energyEqIdx);
        }
        // inner boundary (symmetry axis)
        else if (onLeftBoundary_(globalPos))
        {
            values.setAllSymmetry();
        }

        if (couplingManager().isCoupledEntity(CouplingManager::freeFlowIdx, scvf))
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setCouplingNeumann(Indices::energyEqIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        return initialAtPos(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        static const auto avgType = FreeFlowHeatCouplingOptions::stringToEnum(getParamFromGroup<std::string>(this->paramGroup(), "Problem.DiffCoeffAvgType", "FreeFlowOnly"));
        if (couplingManager().isCoupledEntity(CouplingManager::freeFlowIdx, scvf))
            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, avgType);
        return values;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    /*!
     * \brief Evaluates the initial value for a control volume.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        values[Indices::pressureIdx] = 0.0;
        values[Indices::energyEqIdx] = inletTemperature_;

        // parabolic velocity profile
        const auto radius = globalPos[0] - this->gridGeometry().bBoxMin()[0];
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 2.0*inletVelocity_*(1.0 - radius*radius/(channelRadius_*channelRadius_));
        if (radius > channelRadius_)
            values[Indices::velocityYIdx] = 0.0;
        return values;
    }

    Scalar Re() const { return Re_; }
    Scalar Pr() const { return Pr_; }
    Scalar Pe() const { return Pe_; }
    Scalar RePore() const { return RePore_; }
    Scalar PePore() const { return PePore_; }
    Scalar nu() const { return nu_; }
    Scalar rho() const { return rho_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    static constexpr Scalar eps_ = 1e-12;
    std::string problemName_;

    Scalar channelRadius_;
    Scalar inletVelocity_;
    Scalar inletTemperature_;

    Scalar Re_, Pe_, Pr_, nu_, rho_;
    Scalar RePore_, PePore_;

    std::shared_ptr<CouplingManager> couplingManager_;
};
} // end namespace Dumux

#endif
