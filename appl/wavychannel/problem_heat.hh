// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_HEAT_SUBPROBLEM_HH
#define DUMUX_HEAT_SUBPROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \brief Heat subproblem for conjugate heat transfer
 */
template <class TypeTag>
class HeatSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = Dumux::BoundaryTypes<PrimaryVariables::size()>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    HeatSubProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "SolidEnergy"), couplingManager_(couplingManager)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        enableOuterFluxBC_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableOuterFluxBC", false);
        if (enableOuterFluxBC_)
            outerEnergyFlux_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.OuterEnergyFlux");
        initialTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialTemperature");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolumeFace &scvf) const
    {
        BoundaryTypes values;

        // no-flow at the upstream and downstream boundaries
        if (onUpstreamBoundary_(scvf.center()) || onDownstreamBoundary_(scvf.center()))
            values.setAllNeumann();

        else if (couplingManager().isCoupledEntity(CouplingManager::solidEnergyIdx, scvf))
            values.setAllCouplingNeumann();

        // outer boundary
        else
        {
            if (enableOuterFluxBC_)
                values.setAllNeumann();
            else
                values.setAllDirichlet();
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     */
    template<class ElementVolumeVariables, class ElementFluxVariablesCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        static const auto avgType = FreeFlowHeatCouplingOptions::stringToEnum(getParamFromGroup<std::string>(this->paramGroup(), "Problem.DiffCoeffAvgType", "FreeFlowOnly"));
        if (couplingManager().isCoupledEntity(CouplingManager::solidEnergyIdx, scvf))
            return couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scvf, avgType);
        else if (onUpstreamBoundary_(scvf.center()) || onDownstreamBoundary_(scvf.center()))
            return { 0.0 };
        else
            return { outerEnergyFlux_ };
    }

    /*!
     * \brief Evaluates the Dirichlet value for a control volume boundary.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& pos) const
    {
        PrimaryVariables values(initialTemperature_);
        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& pos) const
    {
        PrimaryVariables values(initialTemperature_);
        return values;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    bool onDownstreamBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    bool onUpstreamBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    static constexpr Scalar eps_ = 1e-12;
    std::string problemName_;
    Scalar outerEnergyFlux_;
    Scalar initialTemperature_;
    bool enableOuterFluxBC_;

    std::shared_ptr<CouplingManager> couplingManager_;

};
} // end namespace Dumux

#endif
