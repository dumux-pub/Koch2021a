// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_CONJUGATE_HEAT_CHANNEL_3D_ROTSYM_PROPERTIES_HH
#define DUMUX_CONJUGATE_HEAT_CHANNEL_3D_ROTSYM_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/common/properties.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/discretization/extrusion.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>

#include "problem_freeflow.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct FreeFlowModel { using InheritsFrom = std::tuple<NavierStokesNI, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FreeFlowModel>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::SimpleH2O<Scalar>>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeFlowModel>
{
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, 2>>;
    using type = Dune::SubGrid<HostGrid::dimension, HostGrid>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::FreeFlowModel>
{ using type = Dumux::FreeFlowSubProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::FreeFlowModel> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::FreeFlowModel> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::FreeFlowModel> { static constexpr bool value = true; };

// rotation-symmetric grid geometry forming a cylinder channel
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::FreeFlowModel>
{
    static constexpr auto upwindSchemeOrder = getPropValue<TypeTag, Properties::UpwindSchemeOrder>();
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;

    struct GGTraits : public StaggeredFreeFlowDefaultFVGridGeometryTraits<GridView, upwindSchemeOrder>
    { using Extrusion = RotationalExtrusion<0>; };

    using type = StaggeredFVGridGeometry<GridView, enableCache, GGTraits>;
};

template<class TypeTag>
struct NormalizePressure<TypeTag, TTag::FreeFlowModel>
{ static constexpr bool value = false; };

} // end namespace Dumux::Properties

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/solidenergy/model.hh>
#include <dumux/material/solidsystems/1csolid.hh>
#include <dumux/material/components/constant.hh>

#include "problem_heat.hh"
#include "spatialparams.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct HeatModel { using InheritsFrom = std::tuple<SolidEnergy, CCTpfaModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::HeatModel> { using type = Dumux::HeatSubProblem<TypeTag>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::HeatModel>
{
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, 2>>;
    using type = Dune::SubGrid<HostGrid::dimension, HostGrid>;
};

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::HeatModel>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePSpatialParams<GridGeometry, Scalar>;
};

// per default the solid system is inert with one constant component
template<class TypeTag>
struct SolidSystem<TypeTag, TTag::HeatModel>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = SolidSystems::InertSolidPhase<Scalar, Components::Constant<2, Scalar>>;
};

// rotation-symmetric grid geometry forming a hollow cylinder around channel
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::HeatModel>
{
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;

    struct GGTraits : public CCTpfaDefaultGridGeometryTraits<GridView>
    { using Extrusion = RotationalExtrusion<0>; };

    using type = CCTpfaFVGridGeometry<GridView, enableCache, GGTraits>;
};

} // end namespace Dumux::Properties

#include <dumux/multidomain/boundary/freeflowsolidenergy/couplingmanager.hh>
#include <dumux/multidomain/staggeredtraits.hh>

namespace Dumux::Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeFlowModel>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::HeatModel>;
    using type = Dumux::FreeFlowSolidEnergyCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::HeatModel>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeFlowModel, Properties::TTag::FreeFlowModel, TypeTag>;
    using type = Dumux::FreeFlowSolidEnergyCouplingManager<Traits>;
};

} // end namespace Dumux::Properties

#endif
