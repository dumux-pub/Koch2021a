#!/usr/bin/env python3

import shutil
import os
import numpy as np
import subprocess
import copy
import uuid

folder = "logfiles3"
#shutil.rmtree("logfiles", ignore_errors=True)
os.makedirs(folder, exist_ok=True)

def run_case(velocity, pore_radius, channel_radius, solid_material):
    name = folder + "/" + str(uuid.uuid4()) + ".log"

    args = [
        "-Problem.LogFileName", name,
        "-Vtk.OutputName", name,
        "-FreeFlow.Problem.InletVelocity", "{}".format(velocity),
        "-Grid.PoreRadius", "{}".format(pore_radius),
        "-Grid.ChannelRadius", "{}".format(channel_radius),
        "-2.Component.SolidThermalConductivity", "{}".format(solid_material[0]),
        "-2.Component.SolidHeatCapacity", "{}".format(solid_material[1]),
        "-2.Component.SolidDensity", "{}".format(solid_material[2]),
    ]

    subprocess.run(["./test_conjugate_heat_channel3drotsym_pore"] + args)
    return name

alu = [205.0, 900.0, 2700.0]
pdms = [0.15, 1460.0, 965.0]
granite = [2.6, 790.0, 2700.0]
fiction = [5.2, 850.0, 2700.0]
fiction2 = [20.5, 850.0, 2700.0]

default_params = {
    "velocity": 0.1,
    "pore_radius": 1.7e-5,
    "channel_radius": 1e-5,
    "solid_material": alu,
}

masterLogFileName = folder + "/" + str(uuid.uuid4()) + ".master.log"
print("Logfile: ", masterLogFileName)

velocities = np.logspace(np.log10(0.0005), np.log10(5.0), 17)

with open(masterLogFileName, "w") as logFile:
    logFile.write("velocities: " + str(velocities) + "\n")
    for velocity in velocities:
        params = copy.deepcopy(default_params)
        params["velocity"] = velocity
        log = run_case(**params)
        logFile.write(log + "\n")
