#!/bin/bash

################################################################################
# This script installs the module "dumux-dualnetwork" together with all dependencies.

# defines a function to exit with error message
exitWith ()
{
    echo "\n$1"
    exit 1
}

# Everything will be installed into a newly created sub-folder named "dumux".
echo "Creating the folder dumux to install the modules in"
if ! mkdir -p dumux; then exitWith "--Error: could not create top folder dumux"; fi
if ! cd dumux; then exitWith "--Error: could not enter top folder dumux"; fi

# dumux-dualnetwork
# master # 76943ccc32e7b40d4c6be46802ea4afbead7302a # 2021-04-29 17:02:55 +0200 # Timo Koch
if ! git clone https://git.iws.uni-stuttgart.de/dumux-pub/Koch2021a.git dumux-dualnetwork; then exitWith "-- Error: failed to clone dumux-dualnetwork."; fi
if ! cd dumux-dualnetwork; then exitWith "-- Error: could not enter folder dumux-dualnetwork."; fi
if ! git checkout master; then exitWith "-- Error: failed to check out branch master in module dumux-dualnetwork."; fi
echo "-- Successfully set up the module dumux-dualnetwork\n"
cd ..

# dune-foamgrid
# releases/2.7 # d49187be4940227c945ced02f8457ccc9d47536a # 2020-01-06 15:36:03 +0000 # Timo Koch
if ! git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git; then exitWith "-- Error: failed to clone dune-foamgrid."; fi
if ! cd dune-foamgrid; then exitWith "-- Error: could not enter folder dune-foamgrid."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-foamgrid."; fi
if ! git reset --hard d49187be4940227c945ced02f8457ccc9d47536a; then exitWith "-- Error: failed to check out commit d49187be4940227c945ced02f8457ccc9d47536a in module dune-foamgrid."; fi
echo "-- Successfully set up the module dune-foamgrid\n"
cd ..

# dumux
# master # bef84a912f0ad90583fbe0d69715db91abb35456 # 2021-04-29 14:57:37 +0000 # Dennis Gläser
if ! git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux; then exitWith "-- Error: failed to clone dumux."; fi
if ! cd dumux; then exitWith "-- Error: could not enter folder dumux."; fi
if ! git checkout master; then exitWith "-- Error: failed to check out branch master in module dumux."; fi
if ! git reset --hard bef84a912f0ad90583fbe0d69715db91abb35456; then exitWith "-- Error: failed to check out commit bef84a912f0ad90583fbe0d69715db91abb35456 in module dumux."; fi
echo "-- Successfully set up the module dumux\n"
cd ..

# dune-grid
# releases/2.7 # b7741c6599528bc42017e25f70eb6dd3b5780277 # 2020-11-26 23:30:08 +0000 # Christoph Grüninger
if ! git clone https://gitlab.dune-project.org/core/dune-grid.git; then exitWith "-- Error: failed to clone dune-grid."; fi
if ! cd dune-grid; then exitWith "-- Error: could not enter folder dune-grid."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-grid."; fi
if ! git reset --hard b7741c6599528bc42017e25f70eb6dd3b5780277; then exitWith "-- Error: failed to check out commit b7741c6599528bc42017e25f70eb6dd3b5780277 in module dune-grid."; fi
echo "-- Successfully set up the module dune-grid\n"
cd ..

# dune-geometry
# releases/2.7 # 9d56be3e286bc761dd5d453332a8d793eff00cbe # 2020-11-26 23:26:48 +0000 # Christoph Grüninger
if ! git clone https://gitlab.dune-project.org/core/dune-geometry.git; then exitWith "-- Error: failed to clone dune-geometry."; fi
if ! cd dune-geometry; then exitWith "-- Error: could not enter folder dune-geometry."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-geometry."; fi
if ! git reset --hard 9d56be3e286bc761dd5d453332a8d793eff00cbe; then exitWith "-- Error: failed to check out commit 9d56be3e286bc761dd5d453332a8d793eff00cbe in module dune-geometry."; fi
echo "-- Successfully set up the module dune-geometry\n"
cd ..

# dune-localfunctions
# releases/2.7 # 68f1bcf32d9068c258707d241624a08b771b6fde # 2020-11-26 23:45:36 +0000 # Christoph Grüninger
if ! git clone https://gitlab.dune-project.org/core/dune-localfunctions.git; then exitWith "-- Error: failed to clone dune-localfunctions."; fi
if ! cd dune-localfunctions; then exitWith "-- Error: could not enter folder dune-localfunctions."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-localfunctions."; fi
if ! git reset --hard 68f1bcf32d9068c258707d241624a08b771b6fde; then exitWith "-- Error: failed to check out commit 68f1bcf32d9068c258707d241624a08b771b6fde in module dune-localfunctions."; fi
echo "-- Successfully set up the module dune-localfunctions\n"
cd ..

# dune-subgrid
# releases/2.7 # 45d1ee9f3f711e209695deee97912f4954f7f280 # 2020-05-28 13:21:59 +0000 # oliver.sander_at_tu-dresden.de
if ! git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git; then exitWith "-- Error: failed to clone dune-subgrid."; fi
if ! cd dune-subgrid; then exitWith "-- Error: could not enter folder dune-subgrid."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-subgrid."; fi
if ! git reset --hard 45d1ee9f3f711e209695deee97912f4954f7f280; then exitWith "-- Error: failed to check out commit 45d1ee9f3f711e209695deee97912f4954f7f280 in module dune-subgrid."; fi
echo "-- Successfully set up the module dune-subgrid\n"
cd ..

# dune-common
# releases/2.7 # aa689abba532f40db8f5663fa379ea77211c1953 # 2020-11-10 13:36:21 +0000 # Christian Engwer
if ! git clone https://gitlab.dune-project.org/core/dune-common.git; then exitWith "-- Error: failed to clone dune-common."; fi
if ! cd dune-common; then exitWith "-- Error: could not enter folder dune-common."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-common."; fi
if ! git reset --hard aa689abba532f40db8f5663fa379ea77211c1953; then exitWith "-- Error: failed to check out commit aa689abba532f40db8f5663fa379ea77211c1953 in module dune-common."; fi
echo "-- Successfully set up the module dune-common\n"
cd ..

# dune-istl
# releases/2.7 # 761b28aa1deaa786ec55584ace99667545f1b493 # 2020-11-26 23:29:21 +0000 # Christoph Grüninger
if ! git clone https://gitlab.dune-project.org/core/dune-istl.git; then exitWith "-- Error: failed to clone dune-istl."; fi
if ! cd dune-istl; then exitWith "-- Error: could not enter folder dune-istl."; fi
if ! git checkout releases/2.7; then exitWith "-- Error: failed to check out branch releases/2.7 in module dune-istl."; fi
if ! git reset --hard 761b28aa1deaa786ec55584ace99667545f1b493; then exitWith "-- Error: failed to check out commit 761b28aa1deaa786ec55584ace99667545f1b493 in module dune-istl."; fi
echo "-- Successfully set up the module dune-istl\n"
cd ..

echo "-- All modules haven been cloned successfully. Configuring project..."
if ! ./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all; then exitWith "--Error: could not configure project"; fi

echo "-- Configuring successful. Compiling applications..."
if ! cd dumux-dualnetwork/build-cmake; then exitWith "--Error: could not enter build directory at dumux-dualnetwork/build-cmake"; fi
if ! make build_tests; then exitWith "--Error: applications could not be compiled. Please try to compile them manually."; fi
